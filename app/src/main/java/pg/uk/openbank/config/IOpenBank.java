package pg.uk.openbank.config;

import android.support.annotation.NonNull;

import pg.uk.openbank.exception.MandatoryException;

/**
 * Created by manmeet on 12/3/18.
 * <p>Used for application level settings configuration</p>
 * <p>This class will work as ExpressionBuilder
 * {@link <ahref https://martinfowler.com/bliki/FluentInterface.html/>}</p>
 *
 * @see OpenBank
 */

public interface IOpenBank {

    /**
     * To setup open bank client id & client secret
     *
     * @param clientId     issues by OB
     * @param clientSecret issues by OB based on clientId
     * @return IOpenBank
     */
    IOpenBank client(@NonNull String clientId, @NonNull String clientSecret);

    /**
     * To setup open bank web api base url
     *
     * @param webApiBaseUrl base url of web api for open bank
     * @return IOpenBank
     */
    IOpenBank apiBaseUrl(@NonNull String webApiBaseUrl);

    /**
     * To setup open bank web api version
     *
     * @param version web api version in the format of /v1/
     * @return IOpenBank
     */
    IOpenBank apiVersion(String version);

    /**
     * To setup open bank redirect url
     *
     * @param uri redirect url registered with open bank
     * @return IOpenBank
     */
    IOpenBank redirectUri(String uri);
    /**
     * To enable logging
     *
     * @param isLogging true if need to print logs
     * @return IOpenBank
     */
    IOpenBank enableLogging(boolean isLogging);

    /**
     * To validate inputs and prepare configuration setup
     */
    void config() throws MandatoryException;

}
