package pg.uk.openbank.config;

import android.support.annotation.NonNull;

/**
 * Created by manmeet on 12/3/18.
 * <p>Used to configure open bank apis android library basic setup.
 * </br> includes api secret key setup, api key setup, base url and other related configuration</p>
 */

public class OpenBank {

    private OpenBank() {
    }

    /**
     * To setup open bank financial id
     *
     * @param openBankFinancialId The unique id of the PSP issued by OB
     * @return IOpenBank
     */
    public static OpenBankBuilder withFcaId(@NonNull String openBankFinancialId) {
        return new OpenBankBuilder(openBankFinancialId);
    }


    /********
     *Getters*
     ********/

    public static String getWebApiBaseUrl() {
        return OpenBankConfig.getWebApiBaseUrl();
    }

    public static String getWebApiVersion() {
        return OpenBankConfig.getWebApiVersion();
    }

    public static String getFcaId() {
        return OpenBankConfig.getFcaId();
    }

    public static String getClientId() {
        return OpenBankConfig.getApiKey();
    }

    public static String getClientSecret() {
        return OpenBankConfig.getApiSecret();
    }

    public static String getRedirectUriString() {
        return OpenBankConfig.getRedirectUriString();
    }

    public static boolean isLogging() {
        return OpenBankConfig.isLogging();
    }
}
