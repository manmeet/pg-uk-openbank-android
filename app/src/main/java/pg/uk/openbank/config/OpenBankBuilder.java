package pg.uk.openbank.config;

import android.support.annotation.NonNull;

import pg.uk.openbank.exception.MandatoryException;

/**
 * Created by manmeet on 12/3/18.
 * <p>Used to build open data configuration</p>
 * <p>This class will work as ExpressionBuilder
 * {@link <ahref https://martinfowler.com/bliki/ExpressionBuilder.html/>}</p>
 */

public class OpenBankBuilder implements IOpenBank {
    private String financialObId;
    private String clientId;
    private String clientSecret;
    private String baseUrl;
    private String apiVersion;
    private String redirectUri;
    private boolean logging;

    OpenBankBuilder(@NonNull String openBankFinancialId) {
        this.financialObId = openBankFinancialId;
    }

    @Override
    public IOpenBank client(@NonNull String clientId, @NonNull String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        return this;
    }

    @Override
    public IOpenBank apiBaseUrl(@NonNull String webApiBaseUrl) {
        this.baseUrl = webApiBaseUrl;
        return this;
    }

    @Override
    public IOpenBank apiVersion(String version) {
        this.apiVersion = version;
        return this;
    }

    @Override
    public IOpenBank redirectUri(String uri) {
        this.redirectUri = uri;
        return this;
    }

    @Override
    public IOpenBank enableLogging(boolean isLogging) {
        this.logging = isLogging;
        return this;
    }

    @Override
    public void config() throws MandatoryException {

        //validate mandatory data
        OpenBankConfigValidator.validateFcaId(getFinancialObId());

        //add configuration for app level
        OpenBankConfig.setFcaId(getFinancialObId());
        OpenBankConfig.setApiKey(getClientId());
        OpenBankConfig.setApiSecret(getClientSecret());
        OpenBankConfig.setWebApiBaseUrl(getBaseUrl());
        OpenBankConfig.setWebApiBaseUrl(getBaseUrl());
        OpenBankConfig.setRedirectUriString(getRedirectUri());
        OpenBankConfig.enableLogging(isLogging());
        //prepare version string for web api client
        OpenBankConfig.setWebApiVersion(OpenBankConfigValidator.resolveApiVersion(getApiVersion(), getBaseUrl()));
    }

    /*******
     *Getters*
     * *****/
    String getFinancialObId() {
        return financialObId;
    }

    String getClientId() {
        return clientId;
    }

    String getClientSecret() {
        return clientSecret;
    }

    String getBaseUrl() {
        return baseUrl;
    }

    String getApiVersion() {
        return apiVersion;
    }

    String getRedirectUri() {
        return redirectUri;
    }

    boolean isLogging() {
        return logging;
    }
}
