package pg.uk.openbank.config;

import android.base.util.ApplicationUtils;
import android.support.annotation.NonNull;

import java.io.File;

import pg.uk.openbank.exception.MandatoryException;

/**
 * Created by manmeet on 12/3/18.
 * <p>Used to add methods to validate developer/user inputs while configuration of library setup</p>
 */

public class OpenBankConfigValidator {

    /**
     * private constructor
     */
    private OpenBankConfigValidator() {
    }

    /**
     * To check for empty or null financial OB id
     *
     * @param fcaId entered financial id
     * @throws MandatoryException
     */
    static void validateFcaId(String fcaId) throws MandatoryException {
        if (ApplicationUtils.Validator.isEmptyOrNull(fcaId)) {
            throw new MandatoryException("FcaId should not be blank or null.\n "
                    + "The unique id of the PSP to which the request is issued. The unique id will be issued by OB");
        }
    }

    /**
     * To check for empty or null parameter
     *
     * @param data user input
     * @throws MandatoryException
     */
    static void validateEmptyOrNull(String data) throws MandatoryException {
        if (ApplicationUtils.Validator.isEmptyOrNull(data)) {
            throw new MandatoryException("data should not be blank or null.");
        }
    }

    /**
     * To make version string valid based on web service client
     *
     * @param apiVersion user entered version string
     * @return updated version string
     */
    static String resolveApiVersion(@NonNull String apiVersion, @NonNull String baseUrl) {
        String version = apiVersion;
        if (!version.startsWith(File.separator)
                && !baseUrl.endsWith(File.separator)) {
            version = File.separator + version;
        }
        if (!version.endsWith(File.separator)) {
            version += File.separator;
        }
        return version;
    }
}
