package pg.uk.openbank.config;

import android.base.http.WebConstant;
import android.base.util.ApplicationUtils;

import pg.uk.openbank.BuildConfig;

/**
 * Created by manmeet on 12/3/18.
 * <p>Contains the configuration variables
 * such as web api base url, application version number, fca id, client id, secret, logs</p>
 */

public class OpenBankConfig {
    private OpenBankConfig() {
    }

    // FCA id
    private static String sFinancialObId = BuildConfig.financialObId;
    // api key
    private static String sApiKey = BuildConfig.clientId;
    // api secret
    private static String sApiSecret = BuildConfig.clientSecret;


    //Api url for dev
    private static String sWebApiBaseUrl = "https://apis-bank-test.apigee.net/apis";
    //Api version for dev
    private static String sWebApiVersion = "/v1.0/";


    //Callback url
    private static String sRedirectUriString = BuildConfig.redirectUriString;


    //Logs enable/disable identifier
    private static boolean sLogging;

    /********
     *Getters & Setters*
     ********/

    static synchronized String getWebApiBaseUrl() {
        return sWebApiBaseUrl;
    }

    static synchronized void setWebApiBaseUrl(String baseWebUrl) {
        sWebApiBaseUrl = baseWebUrl;
        WebConstant.setBaseUrl(getWebApiBaseUrl());
    }

    static synchronized String getWebApiVersion() {
        return sWebApiVersion;
    }

    static synchronized void setWebApiVersion(String webApiVersion) {
        sWebApiVersion = webApiVersion;
        WebConstant.setApiVersion(getWebApiVersion());
    }

    static String getFcaId() {
        return sFinancialObId;
    }

    static void setFcaId(String fcaId) {
        sFinancialObId = fcaId;
    }

    static String getApiKey() {
        return sApiKey;
    }

    static void setApiKey(String apiKey) {
        sApiKey = apiKey;
    }

    static String getApiSecret() {
        return sApiSecret;
    }

    static void setApiSecret(String apiSecret) {
        sApiSecret = apiSecret;
    }

    static String getRedirectUriString() {
        return sRedirectUriString;
    }

    static void setRedirectUriString(String redirectUri) {
        sRedirectUriString = redirectUri;
    }

    /*Enable logging */
    static void enableLogging(boolean isLogging) {
        sLogging = isLogging;
        ApplicationUtils.Log.logEnable(isLogging);
    }

    static boolean isLogging() {
        return sLogging;
    }
}
