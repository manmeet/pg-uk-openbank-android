package pg.uk.openbank;

import android.base.fragment.FragmentManager;
import android.base.http.WebHandler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import okhttp3.Headers;
import pg.uk.openbank.config.OpenBank;
import pg.uk.openbank.exception.MandatoryException;
import pg.uk.openbank.webapi.descriptor.ScopeDescriptor;
import pg.uk.openbank.webapi.model.AuthToken;
import pg.uk.openbank.webapi.model.account.AccountsApiResponseModel;
import pg.uk.openbank.webapi.model.balance.BalancesApiResponseModel;
import pg.uk.openbank.webapi.model.transaction.TransactionsApiResponseModel;
import pg.uk.openbank.webapi.pg.WebApiConstant;
import pg.uk.openbank.webapi.pg.WebApiManager;
import pg.uk.openbank.webapi.pg.WebApiManagerHelper;
import pg.uk.openbank.webapi.pg.sampleapp.WebApiConstantSampleApp;
import pg.uk.openbank.webapi.pg.sampleapp.WebApiManagerSampleApp;
import pg.uk.openbank.webapi.pg.sampleapp.model.Link;
import pg.uk.openbank.webapi.pg.sampleapp.model.LoginResponse;
import retrofit2.Response;

public class SampleActivity extends AppCompatActivity implements
//WebApi callback
        WebHandler.OnWebCallback {

    //Testing access token
    final String accessTokenUserConsentTest = "Bearer yaMEipMomjtNEwbZLWKup0CmXxE8";
    final String accessTokenSampleAppTest = "Basic OVozRk9UR3p2ZkFJQ2gzRlI2SndBRkc3Y0FGQ2pRd286c2VlbTdVZzBiSktnSVJpbQ==";
    final String baseUrlSampleApp = "https://integration.mobileconnect.io/sb1/v2/";

    public final String paymentPayload = "{\n" +
            "    \"acr_values\": \"2\",\n" +
            "    \"state\": \"af0ifjsldkj\",\n" +
            "    \"scope\": \"openid accounts payment\",\n" +
            "    \"aud\": \"https://apis-bank-dev.apigee.net\",\n" +
            "    \"iss\": \"https://openbank.apigee.com\",\n" +
            "    \"redirect_uri\": \"psd2app://app.com\",\n" +
            "    \"client_id\": \"8u5BPRaqKJO1l9zUyM1AifG7YKCYP3Yv\",\n" +
            "    \"response_type\": \"code\",\n" +
            "    \"claims\": {\n" +
            "        \"paymentinfo\": {\n" +
            "            \"challenge_type\": \"SANDBOX_TAN\",\n" +
            "            \"type\": \"sepa_credit_transfer\",\n" +
            "            \"value\": {\n" +
            "                \"amount\": \"1\",\n" +
            "                \"currency\": \"GBP\"\n" +
            "            },\n" +
            "            \"to\": {\n" +
            "                \"account_number\": \"62136000\",\n" +
            "                \"remote_bic\": \"RBOSGB2109H\",\n" +
            "                \"remote_IBAN\": \"GB32ESSE40486562136016\",\n" +
            "                \"remote_name\": \"Open api\"\n" +
            "            },\n" +
            "            \"additional\": {\n" +
            "                \"subject\": \"Mobile Test\",\n" +
            "                \"booking_code\": \"XDS234F\",\n" +
            "                \"booking_date\": \"1520336061\",\n" +
            "                \"value_date\": \"1520336061\"\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "}\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        //configure open bank data
        try {
            OpenBank.withFcaId(BuildConfig.financialObId)
                    //dummy Android app example with hard coded client id & secret
                    .client("8u5BPRaqKJO1l9zUyM1AifG7YKCYP3Yv", "kALVAKKIElJebWGA")
                    //rest api example with below client id & secret
                    .client("Wf4vgE4akVJAxnuYGSsmaQEcBeQ8zsak", "XEeviceBJnpeNzwt")
                    .apiBaseUrl("https://apis-bank-test.apigee.net/ais/open-banking")
                    .apiVersion("v1.0")
                    .redirectUri(BuildConfig.redirectUriString)
                    .enableLogging(true)
                    .config();
        } catch (MandatoryException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
            e.printStackTrace();
        }

        Log.d(getClass().getSimpleName(), "base64 Encoded string = "
                + WebApiManagerHelper.getTokenApiWebAuthorizationHeaderValue("Wf4vgE4akVJAxnuYGSsmaQEcBeQ8zsak", "XEeviceBJnpeNzwt"));


    }

    @Override
    protected void onResume() {
        super.onResume();

//        tokenWebApiSample();
//        balancesWebApiSample();
//        transactionsWebApiSample();
//        accountsWebApiSample();
//        beneficiariesWebApiSample();

//        loginSampleAppWebApi();
        openWebFragment(WebApiManagerHelper.getPaymentUserConsentWebPageUrl());
    }

    private void tokenWebApiSample() {
        String apiUrl = "https://apis-bank-test.apigee.net/apis/v1.0/";
        new WebApiManager(this).getAccessTokenWebCall(apiUrl, ScopeDescriptor.WebApiScopeDef.ACCOUNTS, this, true);
    }

    private void balancesWebApiSample() {
        new WebApiManager(this).getBalancesWebCall(this, accessTokenUserConsentTest, true);
    }

    private void transactionsWebApiSample() {
        new WebApiManager(this).getTransactionsWebCall(this, accessTokenUserConsentTest, true);
    }

    private void accountsWebApiSample() {
        new WebApiManager(this).getAccountsWebCall(this, accessTokenUserConsentTest, true);
    }

    private void beneficiariesWebApiSample() {
        new WebApiManager(this).getBeneficiariesWebCall(this, accessTokenUserConsentTest, true);
    }

    private void openPaymentInitiationUrlBasedOnSampleMobileAppCode(){
        /*From sample mobile app*/
//        https://apis-bank-dev.apigee.net/internal/transfer-consent?sessionid=rrt-012efa4626b1198e1-b-eu-8030-17647985-5
//        https://apis-bank-dev.apigee.net/internal/transfer-consent/login?sessionid=rrt-012efa4626b1198e1-b-eu-8030-17647985-5
//        https://apis-bank-dev.apigee.net/internal/transfer-consent/otp?sessionid=rrt-012efa4626b1198e1-b-eu-8030-17647985-5
//        https://apis-bank-dev.apigee.net/internal/transfer-consent/otp/validate
//        psd2app://app.com?error=undefined&error_description=undefined&state=af0ifjsldkj

        /*From APigee site*/
//        https://apis-bank-test.apigee.net/apis/v1.0/oauth/authorize?response_type=code&client_id=80m90oeSbHLBDMxA3j4glc3Z41rQ7s4Y&state=abcd1234&scope=openid%20accounts&redirect_uri=https://api.enterprise.apigee.com/v1/o/apis-bank/apimodels/accounts-apis-v1-0/templateauths/PSUOAuth2Security/callback&request=eyJhbGciOiJSUzI1NiIsImtpZCI6IjkwMjEwQUJBRCIsImI2NCI6ZmFsc2UsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaWF0IjoiMjAxNy0wNi0xMlQyMDowNTo1MCAwMDowMCIsImh0dHA6XC9cL29wZW5iYW5raW5nLm9yZy51a1wvaXNzIjoiQz1VSywgU1Q9RW5nbGFuZCwgTD1Mb25kb24sIE89QWNtZSBMdGQuIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLm9wZW5iYW5rLmNvbSIsInJlc3BvbnNlX3R5cGUiOiJjb2RlIiwiY2xpZW50X2lkIjoiODBtOTBvZVNiSExCRE14QTNqNGdsYzNaNDFyUTdzNFkiLCJyZWRpcmVjdF91cmkiOiJodHRwczpcL1wvYXBpLmVudGVycHJpc2UuYXBpZ2VlLmNvbVwvdjFcL29cL2FwaXMtYmFua1wvYXBpbW9kZWxzXC9hY2NvdW50cy1hcGlzLXYxLTBcL3RlbXBsYXRlYXV0aHNcL1BTVU9BdXRoMlNlY3VyaXR5XC9jYWxsYmFjayIsInNjb3BlIjoib3BlbmlkIGFjY291bnRzIiwic3RhdGUiOiJhYmNkMTIzNCIsIm5vbmNlIjoiMTUyMTAzMTQ1NzAwMCIsImNsYWltcyI6eyJpZF90b2tlbiI6eyJvcGVuYmFua2luZ19pbnRlbnRfaWQiOnsidmFsdWUiOiJ1cm46b3BlbmJhbms6aW50ZW50OmFjY291bnRzOjBkNGE1NjdmLTJmYTctNGZhZS05NGQwLTM0ZmJlNjhlZjU3YiIsImVzc2VudGlhbCI6dHJ1ZX0sImFjciI6eyJlc3NlbnRpYWwiOnRydWV9fX0sImlhdCI6MTUwNDUyMTQ1NSwiZXhwIjoxNjA0NTI1MDU1fQ.cBpT_mZ-IH5HiJm_adANbYsxnf6HDeGa9odOYiS3qo076wfOYJHPtn3EpaaZS8K8PTJymn67KSDswVfSfhdshjYhYIzgt_yIS7czRnwtM_Qeiwc1z8WwWvz5qG-6bazRlXV1ZGUVVyQqujlLFoi4P9Mx0afuHBS7GwvFtKQiO9TS6Cq6g_E-iuN2jX_pEPI94Eigac1RIp3Wmj35l6zVNsRIhpkGTE8sF0bwuXWbecVjbjBWSlNA_efKjmtOyS60J-P-lesU4oP_3-eUN9lkk1_MbpUZMPzIfMC3io8VP-C6zPyKGL3DPDDfXKvCHu5uOm5XJAB7wOzzMpoC877QuNENWeiLkStEhRbiYqAzZ9ZpqbRwon9w7U-NT4PLWIPUS-pSE-BlDIw2zIpM84ngaj2K4wMfKu7E6HMBvQMaVclbw-sSTuSm9WmPJMuxSdBDMks15vEJnvRgssfQmvv2L03eu-gXqqXmWWPL_ysbWdQpxppmjw2GFpyGdTxtoNOy2JqOORCHTFFVxn8wdL1wZWg--U1PmYN4It7_VwuMGx51FsvtnnHKQl-UxKlMwVZCymOVOLqnW35xzhqSUbnlLYU9Q__yf6tkz473iGtp8OuZQjdCBsTvbDvXy4ES2KtxFMmQ4ahROpdzSDIUcxq8FaTpyNroREJd-FaHHJZ9H-o&nonce=1521031457000
//        https://apis-bank-test.apigee.net/apis/internal/v1.0/loginapp?redirectUri=https://apis-bank-test.apigee.net/apis/internal/v1.0/consentapp/consent&state=rz8Le
//        https://apis-bank-test.apigee.net/apis/internal/v1.0/consentapp/consent?state=IgnaG&userDetails=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdG9rZW4iOiJZV010NURyZDJpZUZFZWl5OEhzbzE3aGF3d0FBQVdKSW1kaWZFaTZfREdkMUQtSGhaaWdGNWNHc25QbkxFa0kiLCJleHBpcmVzX2luIjo2MDQ4MDAsInVzZXIiOnsidXVpZCI6ImZiOGZkZjJhLTE1ZjktMTFlOC1hODM4LTEyNWY5YmU5YzJhOCIsInR5cGUiOiJ1c2VyIiwibmFtZSI6InVzZXIxMjMiLCJjcmVhdGVkIjoxNTE5MTAyNDA0NjM1LCJtb2RpZmllZCI6MTUxOTEwMjQwNDYzNSwidXNlcm5hbWUiOiJ1c2VyMTIzIiwiZW1haWwiOiJ1c2VyMTIzQGdtYWlsLmNvbSIsImFjdGl2YXRlZCI6dHJ1ZSwicGljdHVyZSI6Imh0dHA6Ly93d3cuZ3JhdmF0YXIuY29tL2F2YXRhci85MDA1ZGUyODE1NmI0YzA1NGE5MzMzNjFhMDZjOGFhNCIsIm1ldGFkYXRhIjp7InNpemUiOjQ1Nn19LCJpYXQiOjE1MjEwMzE2NjQsImV4cCI6MTUyMTAzMTk2NH0.XOZtx9arhs8v4N8kR-fKgr3RPpXeCCyiIh5kz0AVOsA
//
//        https://apis-bank-test.apigee.net/apis/internal/v1.0/consentapp/consent?state=rz8Le&userDetails=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdG9rZW4iOiJZV010QUxlT3ZDZUNFZWlVLU1ld3NvRFBtUUFBQVdKSWdGeHVzemlnN2FBdFpKb2FJajV3cUg1dVNoQ1NIeUkiLCJleHBpcmVzX2luIjo2MDQ4MDAsInVzZXIiOnsidXVpZCI6ImZiOGZkZjJhLTE1ZjktMTFlOC1hODM4LTEyNWY5YmU5YzJhOCIsInR5cGUiOiJ1c2VyIiwibmFtZSI6InVzZXIxMjMiLCJjcmVhdGVkIjoxNTE5MTAyNDA0NjM1LCJtb2RpZmllZCI6MTUxOTEwMjQwNDYzNSwidXNlcm5hbWUiOiJ1c2VyMTIzIiwiZW1haWwiOiJ1c2VyMTIzQGdtYWlsLmNvbSIsImFjdGl2YXRlZCI6dHJ1ZSwicGljdHVyZSI6Imh0dHA6Ly93d3cuZ3JhdmF0YXIuY29tL2F2YXRhci85MDA1ZGUyODE1NmI0YzA1NGE5MzMzNjFhMDZjOGFhNCIsIm1ldGFkYXRhIjp7InNpemUiOjQ1Nn19LCJpYXQiOjE1MjEwMjk5OTQsImV4cCI6MTUyMTAzMDI5NH0.woLH5aEUzvqbtPYv65_PPhX_8FfplyU0lW0XisfJ4Z4
//        https://apis-bank-test.apigee.net/apis/internal/v1.0/consentapp/consent?state=rz8Le&userDetails=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdG9rZW4iOiJZV010QUxlT3ZDZUNFZWlVLU1ld3NvRFBtUUFBQVdKSWdGeHVzemlnN2FBdFpKb2FJajV3cUg1dVNoQ1NIeUkiLCJleHBpcmVzX2luIjo2MDQ4MDAsInVzZXIiOnsidXVpZCI6ImZiOGZkZjJhLTE1ZjktMTFlOC1hODM4LTEyNWY5YmU5YzJhOCIsInR5cGUiOiJ1c2VyIiwibmFtZSI6InVzZXIxMjMiLCJjcmVhdGVkIjoxNTE5MTAyNDA0NjM1LCJtb2RpZmllZCI6MTUxOTEwMjQwNDYzNSwidXNlcm5hbWUiOiJ1c2VyMTIzIiwiZW1haWwiOiJ1c2VyMTIzQGdtYWlsLmNvbSIsImFjdGl2YXRlZCI6dHJ1ZSwicGljdHVyZSI6Imh0dHA6Ly93d3cuZ3JhdmF0YXIuY29tL2F2YXRhci85MDA1ZGUyODE1NmI0YzA1NGE5MzMzNjFhMDZjOGFhNCIsIm1ldGFkYXRhIjp7InNpemUiOjQ1Nn19LCJpYXQiOjE1MjEwMjk5OTQsImV4cCI6MTUyMTAzMDI5NH0.woLH5aEUzvqbtPYv65_PPhX_8FfplyU0lW0XisfJ4Z4

        openWebFragment(WebApiManagerHelper.getPaymentUserConsentWebPageUrl(paymentPayload));

    }
    /*********
     *Sample App Related Functionality
     * *******/
    private void loginSampleAppWebApi(){
        new WebApiManagerSampleApp(this).login(this,
                baseUrlSampleApp, accessTokenSampleAppTest, OpenBank.getRedirectUriString(), true);
    }

    private void openWebFragment(String webUrl){
        FragmentManager.with(this, R.id.frame_layout)
                .fragment(ViewWebFragment.init(webUrl, null))
                .build();
    }


    /**
     * Web api handler call back methods
     */
    @Override
    public <T> void onSuccess(@Nullable T t, int i, Response response) {
        Log.d(getClass().getSimpleName(), "onSuccess() -> task = " + i + " pojo = " + t + " response = " + response);
        switch (i) {
            case WebApiConstant.ACCESS_TOKEN_CODE:
                Log.d(getClass().getSimpleName(), "case ACCESS_TOKEN_CODE = " + ((AuthToken) t).getAccessToken());
                break;
            case WebApiConstant.GET_BALANCES_CODE:
                Log.d(getClass().getSimpleName(), "case GET_BALANCES_CODE = " + ((BalancesApiResponseModel) t).getBalancesData().getBalances());
                break;
            case WebApiConstant.GET_TRANSACTIONS_CODE:
                Log.d(getClass().getSimpleName(), "case GET_TRANSACTIONS_CODE = " + ((TransactionsApiResponseModel) t).getTransactionsData().getTransactions());
                break;
            case WebApiConstant.GET_ACCOUNTS_CODE:
                Log.d(getClass().getSimpleName(), "case GET_ACCOUNTS_CODE = " + ((AccountsApiResponseModel) t).getAccountsData().getAccounts());
                break;
            case WebApiConstantSampleApp.SAMPLE_APP_LOGIN_API_METHOD_CODE:
                Log.d(getClass().getSimpleName(), "case SAMPLE_APP_LOGIN_API_METHOD_CODE -> links count = " + ((LoginResponse) t).getLinks().size());
                openWebFragment(((Link)((LoginResponse)t).getLinks().get(0)).getHref());
                break;
            default:
                Log.d(getClass().getSimpleName(), "case default ");
                break;
        }
        Headers headers = response.headers();
        for(int count=1; count <headers.size(); count++){
            Log.d(getClass().getSimpleName(), "response header -> position = "+count+" data = "+headers.value(i)+"\n"+headers.get(WebApiConstant.JWS_SIGNATURE_PARAM));
        }
    }

    @Override
    public <T> void onError(@Nullable T t, String s, int i, Response response) {
        Log.d(getClass().getSimpleName(), "onError() -> error message = " + s + "task = " + i + " pojo = " + t + " response = " + response);
        switch(i){
            case WebApiConstantSampleApp.SAMPLE_APP_LOGIN_API_METHOD_CODE:
//                Log.d(getClass().getSimpleName(), "case SAMPLE_APP_LOGIN_API_METHOD_CODE -> links count = " + ((LoginResponse) t).getLinks().size());
                String body = response.body().toString();
                if(body.contains("<html")){
                    openWebFragment(body);
                }
                break;
        }
    }
}
