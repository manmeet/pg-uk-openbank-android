package pg.uk.openbank.exception;

/**
 * Created by manmeet on 12/3/18.
 * <p>Used as a custom exception to give runtime exception
 * if data is required and user entered blank/null data</p>
 *
 * @see pg.uk.openbank.config.OpenBank
 */

public class MandatoryException extends Exception {
    public MandatoryException(String message) {
        super(message);
    }

}
