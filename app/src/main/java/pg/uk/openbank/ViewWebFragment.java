package pg.uk.openbank;

import android.annotation.TargetApi;
import android.base.alert.Alert;
import android.base.alert.OnSnackBarActionListener;
import android.base.fragment.BaseFragment;
import android.base.fragment.FragParam;
import android.base.fragment.FragmentManager;
import android.base.ui.custom.ProgressViewApplication;
import android.base.ui.widget.BaseWebView;
import android.base.util.ApplicationUtils;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;


/**
 * Created by clickapps on 15/9/16.
 * <p>This fragment is used to display the html/web screen with in the application
 * on to web view</p>
 */
public class ViewWebFragment extends BaseFragment implements
        //snack bar action listener
        OnSnackBarActionListener {
    /*Initiate ui components*/
    RelativeLayout mRootLayout;
    BaseWebView mWebView;

    /*Instance variables*/
    private String mWebUrl = "";
    private static final String WEB_URL_BUNDLE_KEY = "webUrl";
    private static final int TASK_WEB_URL_NOT_PRESENT = 0x0001;

    /**
     * initialize current fragment with assign a Bundle/values pass on calling time
     */
    @SuppressWarnings("Initiate fragment form this method init()")
    public static ViewWebFragment init(String webUrl, Bundle bundle) {
        ViewWebFragment fragment = (ViewWebFragment) init(ViewWebFragment.class, bundle);
        //add web url
        Bundle bundleNew = fragment.getBundle();
        bundleNew.putString(WEB_URL_BUNDLE_KEY, webUrl);
        fragment.setArguments(bundleNew);
        return fragment;
    }

    @Override
    protected View initUI(LayoutInflater inflater, ViewGroup container) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.view_web, null);
            mRootLayout = mView.findViewById(R.id.view_web_root_view);
            mWebView = mView.findViewById(R.id.view_web_webview);

            //check for web url
            resolveWebUrl();
        }
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();

        //load web url
        loadWebUrl();

        //add web clients
        addWebViewClient();
    }

    /**
     * Functionality to get url from bundle
     */
    private void resolveWebUrl() {
        if (ApplicationUtils.Validator.isEmptyOrNull(mWebUrl)) {
            mWebUrl = getBundle().getString(WEB_URL_BUNDLE_KEY, "");
        }
        if (ApplicationUtils.Validator.isEmptyOrNull(getWebUrl())) {
            //url not available
            Alert.with(getActivity(), getString(R.string.error_web_url_not_present), R.color.colorPrimaryDark)
                    .actionMessage(getString(android.R.string.ok))
                    .duration(Snackbar.LENGTH_INDEFINITE)
                    .listener(this)
                    .uniqueId(TASK_WEB_URL_NOT_PRESENT)
                    .show();
        } else {
            //set-up web view
            setupWebView();
        }
    }

    /**
     * Functionality to set-up web view
     */
    @SuppressWarnings("deprecation")
    private void setupWebView() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        }
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setGeolocationEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        /**@see <ahref https://stackoverflow.com/questions/32155634/android-webview-not-loading-mixed-content/>
         @see <ahref https://developers.google.com/web/fundamentals/security/prevent-mixed-content/what-is-mixed-content/>*/
        if (Build.VERSION.SDK_INT >= 21) {
            this.mWebView.getSettings().setMixedContentMode(0);
        }
//        mWebView.addJavascriptInterface();

    }

    /**
     * Functionality to initiate web view client callbacks
     */
    private void addWebViewClient() {
        ProgressViewApplication progressViewAppDialog = new ProgressViewApplication(getActivity());
        final ProgressViewApplication.CustomDialog progressDialog = progressViewAppDialog.getProgressDialog();


        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url,
                                     String message, JsResult result) {
                ApplicationUtils.Log.d(getClass().getSimpleName(), "onJsAlert = " + url);
                return true;
            }
        });

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                // stop progress mDialog
                progressDialog.dismiss();
                super.onPageFinished(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url,
                                      Bitmap favicon) {

                // start progress mDialog
                progressDialog.show();
                super.onPageStarted(view, url, favicon);
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String url) {
                final Uri uri = Uri.parse(url);
                return handleOverrideUrl(uri);
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri uri = request.getUrl();
                return handleOverrideUrl(uri);
            }
        });


    }

    private boolean handleOverrideUrl(final Uri uri) {
        ApplicationUtils.Log.i(getTag(), "handleOverrideUrl() -> Uri =" + uri);
        final String host = uri.getHost();
        final String scheme = uri.getScheme();
        // final String queryParameterCode = uri.getQueryParameter(WebConstant.CODE_PARAM);
//        if(uri.toString().startsWith(Config.getBaseWebUrlOauthCallback())
//                && !ApplicationUtils.Validator.isEmptyOrNull(queryParameterCode)){
//            new WebController(getActivity()).getAccessTokenWebCall(uri, (WebHandler.OnWebCallback)getActivity(), true);
//            return true;
//        }else {
        try {
            if (scheme.endsWith("mp3")) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        uri);
                startActivity(intent);
                return false;
            } else if (scheme.startsWith("http")
                    || scheme.startsWith("https")) {
                return false;
            }

            // Otherwise allow the OS to handle it
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
//        }
        return false;
    }

    /**
     * Functionality to start web url
     */
    @SuppressWarnings("Required Manifest permissions - <uses-permission android:name=\"android.permission.INTERNET\" />\n" +
            "    <uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />")
    private void loadWebUrl() {
        if (ApplicationUtils.Validator.isInternetConnected(getActivity())) {
            //display web url
            if (getWebUrl().contains("<html")) {
                mWebView.loadData(getWebUrl(), "text/html", "utf-8");
            } else {
                mWebView.loadUrl(getWebUrl());
            }
        } else {
            //internet connection not available
            Alert.with(getActivity(), getString(R.string.error_internet_connection), R.color.colorPrimaryDark)
                    .actionMessage(getString(R.string.ok))
                    .show();
        }
    }

    /**
     * Functionality to close current opened fragment
     */
    private void closeCurrentFragment() {
        FragmentManager.with(getFragmentActivity(), -1)
                .type(FragParam.FragType.POP_TAG)
                .tag(ViewWebFragment.class.getSimpleName())
                .build();
    }

    /*******************
     * Snack bar action callback listener*
     *******************/
    @Override
    public void onSnackBarActionClicked(int uniqueId, View view) {
        if (uniqueId == TASK_WEB_URL_NOT_PRESENT) {
            //close this fragment
            closeCurrentFragment();
        }
    }

    /**************
     * Click event call back functionality
     *************/
    @Override
    public void onClick(View v) {
        super.onClick(v);

    }

    /***************************
     * ********Getters***********
     **************************/
    public String getWebUrl() {
        return mWebUrl;
    }
}
