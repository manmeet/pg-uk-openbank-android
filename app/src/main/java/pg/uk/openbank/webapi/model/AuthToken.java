package pg.uk.openbank.webapi.model;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import pg.uk.openbank.webapi.handler.error.WebApiErrorConstant;

/**
 * Created by android.
 * <p>Contains the access token values for the authorised user</p>
 * response format dated - 03/08/2018
 * {
 * "access_token": "SnJvAzE44431uKRZEj9B5LGPDfPTdhinUeNq80pJckxPLK8svyYkaExu9Cu8jZi9",
 * "refresh_token": "u9dZ2SvJ2CaDoquvbgjffzaj8NgxaJ9nfx2Gs4qUwMdywiEZ4bRgKP3XXhlU41qz",
 * "token_type": "Bearer",
 * "expires_in": 86400,
 * "scope": "account:read address:edit address:read balance:read card:read customer:read mandate:delete mandate:read payee:create payee:delete payee:edit payee:read pay-foreign:create pay-local:create transaction:edit transaction:read"
 * }
 */

public class AuthToken implements Serializable {
    @SerializedName("access_token")
    String accessToken;
    @SerializedName("refresh_token")
    String refreshToken;
    @SerializedName("token_type")
    String tokenType;
    @SerializedName("expires_in")
    String expireTimeInSeconds;
    @SerializedName("scope")
    String scope;
    @SerializedName("token_fetched_time_milliseconds")
    String tokenFetchedTimeMilli;

    /***************
     *Getters & Setters*
     **************/
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getTokenFetchedTimeMilli() {
        Optional<String> value = Optional.fromNullable(tokenFetchedTimeMilli);
        return value.isPresent() ? Long.parseLong(value.get()) : WebApiErrorConstant.BLANK_INT;
    }

    public void setTokenFetchedTimeMilli(String tokenFetchedTimeMilli) {
        this.tokenFetchedTimeMilli = tokenFetchedTimeMilli;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getExpireTimeInSeconds() {
        Optional<String> value = Optional.fromNullable(expireTimeInSeconds);
        return value.isPresent() ? Long.parseLong(value.get()) : WebApiErrorConstant.BLANK_INT;
    }

    public void setExpireTimeInSeconds(String expireTimeInSeconds) {
        this.expireTimeInSeconds = expireTimeInSeconds;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
