package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manmeet on 8/3/18.
 * <p>Contains the permission grant types which is defined to access web api data</p>
 */

public class GrantTypeDescriptor {

    // Enumerate valid values for this interface
    @StringDef({WebApiGrantTypeDef.AUTHORIZATION_CODE, WebApiGrantTypeDef.CLIENT_CREDENTIALS})
    @Retention(RetentionPolicy.SOURCE)
            // Describes when the annotation will be discarded
    public @interface WebApiGrantTypeDef { // Create an interface for validating String types
        String AUTHORIZATION_CODE = "authorization_code";
        String CLIENT_CREDENTIALS = "client_credentials";
    }

    /*Define variable for grant type*/
    @WebApiGrantTypeDef
    String grantType;

    /*Define setter for grant type*/
    public void setGrantType(@WebApiGrantTypeDef String grantType) {
        this.grantType = grantType;
    }

    /*Define getter for grant type*/
    @WebApiGrantTypeDef
    public String getGrantType() {
        return grantType;
    }
}
