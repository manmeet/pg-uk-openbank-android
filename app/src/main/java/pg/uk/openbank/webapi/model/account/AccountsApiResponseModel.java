package pg.uk.openbank.webapi.model.account;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pg.uk.openbank.webapi.model.Links;
import pg.uk.openbank.webapi.model.MetaData;

/**
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "  Data": {
 * "Account": [
 * {
 * "AccountId": "22289",
 * "Currency": "GBP",
 * "Nickname": "Bills"
 * }
 * ]
 * },
 * "Links": {
 * "Self": "/accounts/"
 * },
 * "MetaData": {
 * "TotalPages": 1
 * }
 * }
 */

public class AccountsApiResponseModel implements Serializable {
    //basic info
    @SerializedName("Data")
    private AccountsData accountsData;

    //other info
    @SerializedName("Links")
    private Links links;
    @SerializedName("Meta")
    private MetaData metaData;

    public class AccountsData implements Serializable{
        @SerializedName("Account")
        private List<Account> accounts;

        /***************
         *Getters & Setters*
         **************/
        public List<Account> getAccounts() {
            return Optional.fromNullable(accounts).or(new ArrayList<Account>());
        }

        public void setAccounts(List<Account> accounts) {
            this.accounts = accounts;
        }
    }

    /***************
     *Getters & Setters*
     **************/
    public AccountsData getAccountsData() {
        return accountsData;
    }

    public void setAccountsData(AccountsData accountsData) {
        this.accountsData = accountsData;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
