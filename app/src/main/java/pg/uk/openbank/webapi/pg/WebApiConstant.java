package pg.uk.openbank.webapi.pg;

/**
 * <p>Contains the application level constants for web api request & response.
 * All web api url's end points, keys and parameters name defined here.</p>
 */

public class WebApiConstant extends android.base.http.WebConstant {
    /*private constructor because all the members of this class are class level*/
    private WebApiConstant() {
    }

    /**********
     **Web apis**
     * ********/
    static final String ACCESS_TOKEN = "oauth/token";
    public static final int ACCESS_TOKEN_CODE = 0x0001;

    static final String ACCOUNT_REQUEST = "account-requests";
    public static final int ACCOUNT_REQUEST_CODE = 0x0002;

    static final String GET_ACCOUNTS = "accounts";
    public static final int GET_ACCOUNTS_CODE = 0x0003;

    static final String GET_BALANCES = "balances";
    public static final int GET_BALANCES_CODE = 0x0004;

    static final String GET_BENEFICIARIES = "beneficiaries";
    public static final int GET_BENEFICIARIES_CODE = 0x0005;

    static final String GET_TRANSACTIONS = "transactions";
    public static final int GET_TRANSACTIONS_CODE = 0x0006;

    /**********
     **Web apis' parameters**
     * ********/
    //used in header for api requests
    //for #GET_TOKEN - Base 64 encoded string with client_id:client_secret.
    //for other apis - access token
    public static final String AUTHORIZATION_KEY = "Authorization";
    //used as prefix for #WebApiConstant.AUTHORIZATION_KEY header value
    public static final String BEARER = "Bearer";

    public static final String CLIENT_ID_PARAM = "client_id";
    public static final String CLIENT_SECRET_PARAM = "client_secret";
    public static final String REDIRECT_URI_PARAM = "redirect_uri";
    //code param used to extracted query parameter from oauth url callback
    public static final String CODE_PARAM = "code";
    //to request for 'authorization_code' or 'client_credentials' with #GET_TOKEN end point
    public static final String GRANT_TYPE_PARAM = "grant_type";
    //to request for 'accounts' or 'payments' based on AISP/PISP
    //scope is used for grant_type 'client_credentials' with #GET_TOKEN end point
    public static final String SCOPE_PARAM = "scope";
    public static final String CLIENT_ASSERTION_TYPE_PARAM = "client_assertion_type";
    //Authentication JWT token containing clientId as iss and signed using the private key of the TPP with #GET_TOKEN end point
    public static final String CLIENT_ASSERTION_PARAM = "client_assertion";
    //The unique id of the PSP to which the request is issued. The unique id will be issued by OB
    public static final String FAPI_FINANCIAL_ID_PARAM = "x-fapi-financial-id";
    //Detached JWS - [base64(JOSE header)..base64(encrypt(PRIVATE_KEY, base64(payloadJsonString))]
    public static final String JWS_SIGNATURE_PARAM = "x-jws-signature";

    public static final String ID_PARAM = "id";
    public static final String DATA_PARAM = "data";
    public static final String ACCOUNT_NUMBER_PARAM = "accountNumber";
    public static final String NAME_PARAM = "name";
    public static final String TYPE_PARAM = "type";
    public static final String SORT_CODE_PARAM = "sortCode";
    //to make payment on other account within bank
    public static final String DESTINATION_ACCOUNT_ID_PARAM = "destinationAccountUid";
    public static final String PAYMENT_PARAM = "payment";
    public static final String PAYMENT_COMMENT_REFERENCE_PARAM = "reference";

    /**********
     **Web apis' parameters default value**
     * ********/
    public static final String JWT = "JWT";
    public static final String HS256 = "HS256";

    /**********
     **Web apis' shared preference keys**
     * ********/
    public static final String ACCESS_TOKEN_PREFERENCE_KEY = "accessToken";
}
