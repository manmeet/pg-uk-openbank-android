package pg.uk.openbank.webapi.model.transaction;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manmeet on 8/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "BankTransactionCode": {
 * "Code": "ReceivedCreditTransfer",
 * "SubCode": "DomesticCreditTransfer"
 * }
 */

public class BankTransactionCode implements Serializable {
    @SerializedName("Code")
    String code;
    @SerializedName("SubCode")
    String subCode;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }
}
