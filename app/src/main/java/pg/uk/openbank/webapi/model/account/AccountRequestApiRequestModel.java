package pg.uk.openbank.webapi.model.account;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manmeet on 9/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "Data": {
 * "Permissions": [
 * "ReadAccountsDetail",
 * "ReadBalances",
 * "ReadBeneficiariesDetail",
 * "ReadDirectDebits",
 * "ReadProducts",
 * "ReadStandingOrdersDetail",
 * "ReadTransactionsCredits",
 * "ReadTransactionsDebits",
 * "ReadTransactionsDetail"
 * ],
 * "ExpirationDateTime": "2017-05-02T00:00:00+00:00",
 * "TransactionFromDateTime": "2017-05-03T00:00:00+00:00",
 * "TransactionToDateTime": "2017-12-03T00:00:00+00:00"
 * },
 * "Risk": {}
 * }
 */

public class AccountRequestApiRequestModel implements Serializable {
    @SerializedName("Data")
    AccountRequestApiRequestData data;

    public class AccountRequestApiRequestData implements Serializable {
        @SerializedName("ExpirationDateTime")
        String expirationDateTime;
        @SerializedName("TransactionFromDateTime")
        String transactionFromDateTime;
        @SerializedName("TransactionToDateTime")
        String transactionToDateTime;
        @SerializedName("Permissions")
        String[] permissions;

        /***************
         *Getters & Setters*
         **************/
        public String getExpirationDateTime() {
            return expirationDateTime;
        }

        public void setExpirationDateTime(String expirationDateTime) {
            this.expirationDateTime = expirationDateTime;
        }

        public String getTransactionFromDateTime() {
            return transactionFromDateTime;
        }

        public void setTransactionFromDateTime(String transactionFromDateTime) {
            this.transactionFromDateTime = transactionFromDateTime;
        }

        public String getTransactionToDateTime() {
            return transactionToDateTime;
        }

        public void setTransactionToDateTime(String transactionToDateTime) {
            this.transactionToDateTime = transactionToDateTime;
        }

        public String[] getPermissions() {
            return Optional.fromNullable(permissions).or(new String[1]);
        }

        public void setPermissions(String[] permissions) {
            this.permissions = permissions;
        }
    }

    /***************
     *Getters & Setters*
     **************/
    public AccountRequestApiRequestData getData() {
        return data;
    }

    public void setData(AccountRequestApiRequestData data) {
        this.data = data;
    }
}
