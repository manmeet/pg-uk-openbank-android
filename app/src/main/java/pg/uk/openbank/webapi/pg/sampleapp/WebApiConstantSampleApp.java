package pg.uk.openbank.webapi.pg.sampleapp;

/**
 * Created by manmeet on 14/3/18.
 * <p>Contains the web api related constants based on sample app (Android sample application) code provided by Apigee </p>
 */

public class WebApiConstantSampleApp {
    private WebApiConstantSampleApp(){}

    /**********
     **Web apis**
     * ********/
    static final String SAMPLE_APP_LOGIN_API_METHOD = "discovery";
    public static final int SAMPLE_APP_LOGIN_API_METHOD_CODE = 0x0011;


    /**********
     **Web apis' parameters**
     * ********/
    public static final String REDIRECT_URI_PARAM = "Redirect_URL";
}
