package pg.uk.openbank.webapi.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <p>Contains the variables that used for the server response having web api url and tempted info.
 * <br/> initially used on #Account</p>
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "Links": {
 * "Self": "/accounts/22289/transactions/",
 * "Last": "/accounts/22289/transactions?pg=20",
 * "First": "/accounts/22289/transactions/",
 * "Next": "/accounts/22289/transactions?pg=2"
 * },
 */

public class Links implements Serializable {
    @SerializedName("Self")
    String self;
    @SerializedName("Last")
    String last;
    @SerializedName("First")
    String first;
    @SerializedName("Next")
    String next;
    @SerializedName("Prev")
    String previous;

    /***************
     *Getters & Setters*
     **************/
    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }
}
