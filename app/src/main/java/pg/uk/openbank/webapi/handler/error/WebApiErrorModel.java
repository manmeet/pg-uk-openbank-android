package pg.uk.openbank.webapi.handler.error;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

/**
 * {
 * "ErrorResponseCode": "401",
 * "ErrorDescription": "Missing/Invalid Access Token"
 * }
 */
public class WebApiErrorModel {

    /*Instance variables*/
    @SerializedName("success")
    private boolean status;

    @SerializedName("error")
    private String message;

    @SerializedName("ErrorResponseCode")
    private String errorId;

    @SerializedName("error_link")
    private String errorLink;

    @SerializedName("ErrorDescription")
    private String messageDescription;


    /********
     *Getters & Setters*
     * ******/

    public WebApiErrorModel(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getErrorId() {
        Optional<String> value = Optional.fromNullable(errorId);
        return value.isPresent() ? Integer.parseInt(errorId) : WebApiErrorConstant.BLANK_INT;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public String getErrorLink() {
        return errorLink;
    }

    public void setErrorLink(String errorLink) {
        this.errorLink = errorLink;
    }

    public String getMessageDescription() {
        return messageDescription;
    }

    public void setMessageDescription(String messageDescription) {
        this.messageDescription = messageDescription;
    }
}
