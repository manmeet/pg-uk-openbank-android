package pg.uk.openbank.webapi.handler;

import android.app.Activity;
import android.base.http.Builder;
import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import pg.uk.openbank.webapi.handler.error.WebApiErrorController;
import pg.uk.openbank.webapi.handler.error.WebApiErrorModel;
import pg.uk.openbank.webapi.pg.WebApiConstant;
import pg.uk.openbank.webapi.pg.WebApiManagerHelper;
import retrofit2.Response;


/**
 */
public class WebApiController extends WebApiErrorController {

    /**
     * Call web apis
     *
     * @param webApi Enum class where api is being executed
     * @param client WebBuilder
     */
    public static <T, E extends Enum<E> & WebApiInterface> T apiCall(E webApi, Builder client) {
        apiCall(webApi, client, true);
        return null;
    }

    /**
     * Call web apis
     *
     * @param webApi                 Enum class where api is being executed
     * @param client                 WebBuilder
     * @param isDefaultParamRequired true if default parameters are required in every apis
     */
    public static <T, E extends Enum<E> & WebApiInterface> T apiCall(E webApi, Builder client, boolean isDefaultParamRequired) {
        if (webApi != null
                && client != null) {

            if (isDefaultParamRequired) {
                //add default header parameters for all apis
                addDefaultHeaderParam(client);
            }

            // Divert callback from this screen to calling screen.
            // So that we can handle error messages directly from one place for throughout the application
            //TODO:// If not need to handle error msg then just comment this line of code
            client.callback(new WebCallBack(client.getWebParam().getActivityContext(), client.getWebParam().getCallback()));
            return webApi.execute(client);
        }
        return null;
    }


    /**
     * Functionality to add default parameters for request
     *
     * @param client
     */
    public static void addDefaultHeaderParam(Builder client) {
        if (client != null
                && client.getWebParam() != null) {
            Map<String, String> headerParams = client.getWebParam().getHeaderParam();
            if (headerParams == null) {
                headerParams = new LinkedHashMap<>();
            }
            //add access token in header. get from shared preferences
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, WebApiManagerHelper.getAccessTokenForWebApiRequest(client.getWebParam().getContext()));
            //add fapi-financial-id in header. get from shared preferences
            headerParams.put(WebApiConstant.FAPI_FINANCIAL_ID_PARAM, WebApiManagerHelper.getFinancialObId());
        }
    }

    /**
     * Web api response call back
     * <p>used to handle web error codes that is the demands of this app</p>
     * <p>error code 502</p>
     * <p>status 0 for agent to signout from app</p>
     */

    public static class WebCallBack implements WebHandler.OnWebCallback {
        //To identify the calling screen callback so that
        //after doing task and if needed send a callback on to calling screen
        WebHandler.OnWebCallback callingScreenCallBack;
        private Activity activity;

        public WebCallBack(Activity activity, WebHandler.OnWebCallback callingScreenCallBack) {
            this.activity = activity;
            this.callingScreenCallBack = callingScreenCallBack;
        }


        @Override
        public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
            //send callback on calling screen
            //at first check that in response status must be true otherwise
            //send callback for onError
            if (callingScreenCallBack != null) {
                callingScreenCallBack.onSuccess(object, taskId, response);
            }
        }

        @Override
        public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
            errorResponse(activity, callingScreenCallBack, object, error, taskId, response);
        }
    }

    /**
     * Functionality after getting error in response from web apis
     *
     * @param activity              Activity object
     * @param callingScreenCallBack WebHandler.OnWebCallback
     * @param dataModel             generic type class
     * @param errorMsg              message
     * @param taskId                web call task id #webparam
     */
    public static <T> void errorResponse(Activity activity,
                                         WebHandler.OnWebCallback callingScreenCallBack,
                                         T dataModel,
                                         String errorMsg, int taskId,
                                         Response response) {
        //now check error codes for further functionality
        int code = -1;
        if (response != null)
            code = response.code();
        errorMsg = resolveErrorCodes(activity, dataModel, errorMsg, taskId, code);

        //send callback on calling screen
        if (callingScreenCallBack != null) {
            callingScreenCallBack.onError((dataModel == null || dataModel instanceof String)
                            ? new WebApiErrorModel(errorMsg) : dataModel,
                    errorMsg, taskId, response);
        }
    }

}
