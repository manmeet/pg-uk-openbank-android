package pg.uk.openbank.webapi.model.balance;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pg.uk.openbank.webapi.model.Links;
import pg.uk.openbank.webapi.model.MetaData;

/**
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "Data": {
 * "Balance": [{
 * "AccountId": "22289",
 * "Amount": {
 * "Amount": "1230.00",
 * "Currency": "GBP"
 * },
 * "CreditDebitIndicator": "Credit",
 * "Type": "InterimAvailable",
 * "DateTime": "2017-04-05T10:43:07+00:00",
 * "CreditLine": [{
 * "Included": true,
 * "Amount": {
 * "Amount": "1000.00",
 * "Currency": "GBP"
 * },
 * "Type": "Pre-Agreed"
 * }]
 * }]
 * },
 * "Links": {
 * "Self": "/accounts/22289/balances/"
 * },
 * "Meta": {
 * "TotalPages": 1
 * }
 * }
 */

public class BalancesApiResponseModel implements Serializable {
    //basic info
    @SerializedName("Data")
    private BalancesData balancesData;

    //other info
    @SerializedName("Links")
    private Links links;
    @SerializedName("Meta")
    private MetaData metaData;

    public class BalancesData implements Serializable {
        @SerializedName("Balance")
        private List<Balance> balances;

        /***************
         *Getters & Setters*
         **************/
        public List<Balance> getBalances() {
            return Optional.fromNullable(balances).or(new ArrayList<Balance>());
        }

        public void setBalances(List<Balance> balances) {
            this.balances = balances;
        }
    }

    /***************
     *Getters & Setters*
     **************/
    public BalancesData getBalancesData() {
        return balancesData;
    }

    public void setBalancesData(BalancesData balancesData) {
        this.balancesData = balancesData;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
