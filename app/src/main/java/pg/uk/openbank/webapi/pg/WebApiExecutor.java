package pg.uk.openbank.webapi.pg;

import android.base.http.Builder;
import android.base.http.RetrofitManager;

import java.util.Map;

import pg.uk.openbank.webapi.handler.WebApiInterface;
import pg.uk.openbank.webapi.model.AuthToken;
import pg.uk.openbank.webapi.model.account.AccountRequestApiResponseModel;
import pg.uk.openbank.webapi.model.account.AccountsApiResponseModel;
import pg.uk.openbank.webapi.model.balance.BalancesApiResponseModel;
import pg.uk.openbank.webapi.model.beneficiary.BeneficiariesApiResponseModel;
import pg.uk.openbank.webapi.model.transaction.TransactionsApiResponseModel;

/**
 * <p> executor to handle all the web api request call to put in enqueue via retrofit</p>
 */

public enum WebApiExecutor implements WebApiInterface {

    //create and get access token
    CREATE_ACCESS_TOKEN() {
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApi.class).createAccessToken((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AuthToken>(builder.getWebParam()));
            return null;
        }
    },
    //get accounts
    GET_ACCOUNTS(){
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApi.class).getAccounts((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccountsApiResponseModel>(builder.getWebParam()));
            return null;
        }
    },
    //get account's balances
    GET_BALANCES(){
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApi.class).getBalances((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<BalancesApiResponseModel>(builder.getWebParam()));
            return null;
        }
    },
    //get account's payment transaction history
    GET_TRANSACTIONS(){
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApi.class).getTransactions((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<TransactionsApiResponseModel>(builder.getWebParam()));
            return null;
        }
    },
    //get account's payee/beneficiaries
    GET_BENEFICIARIES(){
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApi.class).getBeneficiaries((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<BeneficiariesApiResponseModel>(builder.getWebParam()));
            return null;
        }
    },
    //generate account request token
    ACCOUNT_REQUEST(){
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApi.class).createAccountRequest((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccountRequestApiResponseModel>(builder.getWebParam()));
            return null;
        }
    }
}
