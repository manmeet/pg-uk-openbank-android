package pg.uk.openbank.webapi.pg;

import android.base.preferences.SharedPreferenceApp;
import android.base.util.ApplicationUtils;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pg.uk.openbank.BuildConfig;
import pg.uk.openbank.SampleActivity;
import pg.uk.openbank.config.OpenBank;
import pg.uk.openbank.webapi.descriptor.ScopeDescriptor;

/**
 * Created by android on 7/8/17.
 * <p>Contains the helper method for web api call and response</p>
 */

public class WebApiManagerHelper {

    //Logging identifier
    private static final String TAG = WebApiManagerHelper.class.getSimpleName();

    /*private constructor because all the members of this class are of class level*/
    private WebApiManagerHelper() {
    }

    /**
     * check if access tolen is already available
     *
     * @param context
     * @return true if access token is valid and available else false
     */
    public static boolean isAccessTokenAvailable(@NonNull Context context) {
        String accessToken = new SharedPreferenceApp(context).getString(WebApiConstant.ACCESS_TOKEN_PREFERENCE_KEY, "");
        return ApplicationUtils.Validator.isEmptyOrNull(accessToken);
    }

    /**
     * Functionality to add access token in preferences
     *
     * @param context     Context
     * @param accessToken string to save in preferences
     */
    public static void saveAccessTokenPref(@NonNull Context context, String accessToken) {
        new SharedPreferenceApp(context).save(WebApiConstant.ACCESS_TOKEN_PREFERENCE_KEY, accessToken);
    }

    /**
     * Functionality to get saved access token from preferences
     *
     * @param context Context
     * @return saved access token if access token available else blank string
     */
    public static String getAccessTokenPref(@NonNull Context context) {
        return new SharedPreferenceApp(context).getString(WebApiConstant.ACCESS_TOKEN_PREFERENCE_KEY, "");
    }

    /**
     * Functionality to assemble and return url for web api
     *
     * @return OpenBank.getWebApiBaseUrl() + OpenBank.getWebApiVersion()</p>
     */
    public static String getResolvedWebApiUrl() {
        return OpenBank.getWebApiBaseUrl() + OpenBank.getWebApiVersion();
    }

    /**
     * Functionality to create authorization header key for token api
     *
     * @return AUTHORIZATION header value string
     */
    public static String getTokenWebApiAuthorizationHeaderValue() {
        return getTokenApiWebAuthorizationHeaderValue(OpenBank.getClientId(), OpenBank.getClientSecret());
    }

    /**
     * Functionality to create authorization header key for token api
     *
     * @return AUTHORIZATION header value string
     */
    public static String getTokenApiWebAuthorizationHeaderValue(@NonNull String clientId, @NonNull String clientSecret) {
        return ApplicationUtils.Security.encodeBase64(clientId + ":" + clientSecret, Base64.NO_WRAP);
    }

    /**
     * Functionality to get access token for web api calls' header
     *
     * @param context
     * @return access token string for web api request
     */
    public static String getAccessTokenForWebApiRequest(@NonNull Context context) {
        return WebApiConstant.BEARER
                + " "
                + WebApiManagerHelper.getAccessTokenPref(context);
    }

    /**
     * Functionality to get PSP financial id issues by OB
     * <p>The unique id of the PSP to which the request is issued. The unique id will be issued by OB</p>
     *
     * @return financial id string provided at the time of configuration
     */
    public static String getFinancialObId() {
        return BuildConfig.financialObId;
    }


    /**
     * Functionality to create jws token
     * {@link <ahref https://github.com/jwtk/jjwt/>}
     *
     * @param payload body/claims
     * @return created jwt token or null
     */
    public static String createJwsToken(String payload) {
        Map<String, Object> headerParams = new HashMap<>();
        headerParams.put(Header.TYPE, Header.JWT_TYPE);
        headerParams.put(JwsHeader.ALGORITHM, SignatureAlgorithm.HS256);
        return createJwsToken(headerParams, OpenBank.getClientSecret(), payload);
    }

    /**
     * Functionality to create jws token
     * {@link <ahref https://github.com/jwtk/jjwt/>}
     *
     * @param signingKey private key
     * @param payload    body/claims
     * @return created jwt token or null
     */
    public static String createJwsToken(String signingKey, String payload) {
        Map<String, Object> headerParams = new HashMap<>();
        headerParams.put(Header.TYPE, Header.JWT_TYPE);
        headerParams.put(JwsHeader.ALGORITHM, SignatureAlgorithm.HS256);
        return createJwsToken(headerParams, signingKey, payload);
    }


    /**
     * Functionality to create jws token
     * {@link <ahref https://github.com/jwtk/jjwt/>}
     *
     * @param headers    header string
     * @param signingKey private key
     * @param payload    body/claims
     * @return created jwt token or null
     */
    public static String createJwsToken(Map<String, Object> headers, String signingKey, String payload) {
        String jwsToken = null;
        try {
            jwsToken = Jwts.builder().setPayload(payload)
                    .setHeaderParams(headers)
                    .signWith(SignatureAlgorithm.HS256,
                            ApplicationUtils.Security.encodeBase64(signingKey, Base64.NO_WRAP))
                    .compact();
        } catch (Exception e) {
            e.printStackTrace();
            ApplicationUtils.Log.e(TAG, e.getMessage());
        }
        ApplicationUtils.Log.d(WebApiManagerHelper.class.getSimpleName(), "Jws token created -> " + jwsToken);
        return jwsToken;
    }

    /**
     * To get the user consent web url for payment
     *
     * @param payload body param in json string format {@link SampleActivity#paymentPayload}
     * @return constructed web url for user consent
     */
    public static String getPaymentUserConsentWebPageUrl(String payload) {
        Uri uri = Uri.parse("https://apis-bank-dev.apigee.net/apis/v1/transfers/initiate")
                .buildUpon()
                .appendQueryParameter(WebApiConstant.CLIENT_ID_PARAM, "8u5BPRaqKJO1l9zUyM1AifG7YKCYP3Yv")
                .appendQueryParameter(WebApiConstant.REDIRECT_URI_PARAM, "psd2app://app.com")
                .appendQueryParameter(WebApiConstant.SCOPE_PARAM, new ScopeDescriptor().toString())
                .appendQueryParameter("ui-locales", Locale.ENGLISH.getLanguage())
                .appendQueryParameter("state", "af0ifjsldkj")
                .appendQueryParameter("acr_values", "2")
                .appendQueryParameter("request", createJwsToken("kALVAKKIElJebWGA", payload))
                .build();
        Log.d(TAG, "getPaymentUserConsentWebPageUrl() -> url = " + uri.toString());
        return uri.toString();
    }

    /**
     * To get the user consent web url for payment
     *
     * @param payload body param in json string format {@link SampleActivity#paymentPayload}
     * @return constructed web url for user consent
     */
    public static String getPaymentUserConsentWebPageUrl() {
        //redirectUri=https://apis-bank-test.apigee.net/apis/internal/v1.0/consentapp/consent&state=rz8Le
        Uri uri = Uri.parse("https://apis-bank-test.apigee.net/apis/internal/v1.0/loginapp")
                .buildUpon()
//                .appendQueryParameter(WebApiConstant.CLIENT_ID_PARAM, "8u5BPRaqKJO1l9zUyM1AifG7YKCYP3Yv")
                .appendQueryParameter("redirectUri", "https://apis-bank-test.apigee.net/apis/internal/v1.0/consentapp/consent")
//                .appendQueryParameter(WebApiConstant.SCOPE_PARAM, new ScopeDescriptor().toString())
                //.appendQueryParameter("ui-locales", Locale.ENGLISH.getLanguage())
                .appendQueryParameter("state", "IgnaG")
                //.appendQueryParameter("acr_values", "2")
                //.appendQueryParameter("request", createJwsToken("kALVAKKIElJebWGA", new SampleActivity().paymentPayload))
                .build();
        Log.d(TAG, "getPaymentUserConsentWebPageUrl() -> url = " + uri.toString());
        return uri.toString();
    }
}
