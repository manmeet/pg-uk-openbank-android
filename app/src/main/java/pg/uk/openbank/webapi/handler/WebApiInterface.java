package pg.uk.openbank.webapi.handler;

import android.base.http.Builder;

/**
 * <p>Used for WebApiExecutor enum (web calls) for different module of the app</p>
 */
public interface WebApiInterface {

    /**
     * template method
     * used to call web api for respective api enum code from class
     *
     * @param builder #WebConnect.ApiClient to generate new RetrofitUtil().createService
     * @return <T> the model object after completion of web call
     */
    @SuppressWarnings("unchecked")
    public <T> T execute(Builder builder);
}
