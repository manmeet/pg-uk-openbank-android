package pg.uk.openbank.webapi.model.transaction;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pg.uk.openbank.webapi.model.Links;
import pg.uk.openbank.webapi.model.MetaData;

/**
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 "Data": {
 "Transaction": [
 {
 "AccountId": "22289",
 "TransactionId": "123",
 "TransactionReference": "Ref 1",
 "Amount": {
 "Amount": "10.00",
 "Currency": "GBP"
 },
 "CreditDebitIndicator": "Credit",
 "Status": "Booked",
 "BookingDateTime": "2017-04-05T10:43:07+00:00",
 "ValueDateTime": "2017-04-05T10:45:22+00:00",
 "TransactionInformation": "Cash from Aubrey",
 "BankTransactionCode": {
 "Code": "ReceivedCreditTransfer",
 "SubCode": "DomesticCreditTransfer"
 },
 "ProprietaryBankTransactionCode": {
 "Code": "Transfer",
 "Issuer": "AlphaBank"
 },
 "Balance": {
 "Amount": {
 "Amount": "230.00",
 "Currency": "GBP"
 },
 "CreditDebitIndicator": "Credit",
 "Type": "InterimBooked"
 }
 }
 ]
 },
 "Links": {
 "Self": "/accounts/22289/transactions/"
 },
 "Meta": {
 "TotalPages": 1,
 "FirstAvailableDateTime": "2017-05-03T00:00:00+00:00",
 "LastAvailableDateTime": "2017-12-03T00:00:00+00:00"
 }
 }
 */

public class TransactionsApiResponseModel implements Serializable {
    //basic info
    @SerializedName("Data")
    private TransactionsData transactionsData;

    //other info
    @SerializedName("Links")
    private Links links;
    @SerializedName("Meta")
    private MetaData metaData;

    public class TransactionsData implements Serializable {
        @SerializedName("Transaction")
        private List<Transaction> transactions;

        /***************
         *Getters & Setters*
         **************/
        public List<Transaction> getTransactions() {
            return Optional.fromNullable(transactions).or(new ArrayList<Transaction>());
        }

        public void setTransactions(List<Transaction> transactions) {
            this.transactions = transactions;
        }
    }

    /***************
     *Getters & Setters*
     **************/
    public TransactionsData getTransactionsData() {
        return transactionsData;
    }

    public void setTransactionsData(TransactionsData transactionsData) {
        this.transactionsData = transactionsData;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
