package pg.uk.openbank.webapi.model.account;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manmeet on 9/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "CreditorAccount": {
 * "SchemeName": "SortCodeAccountNumber",
 * "Identification": "80200112345678",
 * "Name": "Mrs Juniper"
 * }
 */

public class CreditorAccount implements Serializable {
    @SerializedName("SchemeName")
    String schemeName;
    @SerializedName("Identification")
    String identification;
    @SerializedName("Name")
    String name;

    /***************
     *Getters & Setters*
     **************/
    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
