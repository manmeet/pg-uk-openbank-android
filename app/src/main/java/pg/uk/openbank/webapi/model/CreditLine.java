package pg.uk.openbank.webapi.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * Created by manmeet on 8/3/18.
 * "CreditLine": [{
 * "Included": true,
 * "Amount": {
 * "Amount": "1000.00",
 * "Currency": "GBP"
 * },
 * "Type": "Pre-Agreed"
 * }]
 */

public class CreditLine implements Serializable {
    @SerializedName("Included")
    boolean included;
    @SerializedName("Amount")
    Money amount;
    @SerializedName("Type")
    String type;

    /***************
     *Getters & Setters*
     **************/
    public boolean isIncluded() {
        return included;
    }

    public void setIncluded(boolean included) {
        this.included = included;
    }

    public Money getAmount() {
        return amount;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
