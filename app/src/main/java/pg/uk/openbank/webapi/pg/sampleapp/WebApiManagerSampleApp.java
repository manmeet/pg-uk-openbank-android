package pg.uk.openbank.webapi.pg.sampleapp;

import android.app.Activity;
import android.base.http.Builder;
import android.base.http.WebConnect;
import android.base.http.WebHandler;
import android.base.http.WebParam;
import android.base.util.ApplicationUtils;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import pg.uk.openbank.webapi.handler.WebApiController;
import pg.uk.openbank.webapi.handler.WebApiControllerModule;
import pg.uk.openbank.webapi.handler.error.WebApiErrorModel;
import pg.uk.openbank.webapi.pg.WebApiConstant;
import pg.uk.openbank.webapi.pg.sampleapp.model.LoginResponse;
import retrofit2.Response;

/**
 * Created by manmeet on 14/3/18.
 * <p>To call web apis based on sample app (Android sample application) code provided by Apigee </p>
 */

public class WebApiManagerSampleApp extends WebApiControllerModule {

    //Logging identifier
    private static final String TAG = WebApiManagerSampleApp.class.getSimpleName();
    /**
     * Constructor definition
     *
     * @param activity Activity
     */
    public WebApiManagerSampleApp(Activity activity) {
        setActivity(activity);
    }


    /**
     * Functionality to call web api to login based on sample app
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          Web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param redirectUri      redirect uri string
     * @param isDialog         true if need to display dialog
     */
    public void login(WebHandler.OnWebCallback responseCallback,
                      @NonNull String baseUrl, @NonNull String accessToken,
                      @NonNull String redirectUri, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstantSampleApp.SAMPLE_APP_LOGIN_API_METHOD_CODE);

            /*Header params*/
            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, accessToken);


            /*Request params*/
            Map<String, Object> params = new LinkedHashMap<>();
            //add financial id in header
            params.put(WebApiConstantSampleApp.REDIRECT_URI_PARAM, redirectUri);

            Builder builder = WebConnect.with(getActivity(), WebApiConstantSampleApp.SAMPLE_APP_LOGIN_API_METHOD)
                    .callback(this, LoginResponse.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .headerParam(headerParams)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutorSampleApp.SAMPLE_APP_LOGIN_API, builder, false);
        } else {
            ApplicationUtils.Log.d(TAG, "login() -> activity object is null");
        }
    }

    /*****************************
     * Web server response functionality
     ****************************/
    @Override
    public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
        super.onSuccess(object, taskId, response);
        if (getCallingScreenWebApiResponseCallback() != null) {
            getCallingScreenWebApiResponseCallback().onSuccess(object, taskId, response);
        }
    }

    @Override
    public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
        super.onError(object, error, taskId, response);
        if (getCallingScreenWebApiResponseCallback() != null) {
            getCallingScreenWebApiResponseCallback().onError(object, error, taskId, response);
        }
    }
}
