package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manmeet on 8/3/18.
 * <p>Contains the transaction codes Transfer/ReceivedCreditTransfer</p>
 */

public class TransactionCodeDescriptor {

    // Enumerate valid values for this interface
    @StringDef({WebApiTransactionCodeDef.TRANSFER, WebApiTransactionCodeDef.RECEIVED_CREDIT_TRANSFER})
    @Retention(RetentionPolicy.SOURCE)
    // Describes when the annotation will be discarded
    public @interface WebApiTransactionCodeDef { // Create an interface for validating String codes
        String TRANSFER = "Transfer";
        String RECEIVED_CREDIT_TRANSFER = "ReceivedCreditTransfer";
    }

    /*Define variable for transaction code*/
    @WebApiTransactionCodeDef
    String transactionCode;

    /*Define setter for transaction code*/
    public void setTransactionCode(@WebApiTransactionCodeDef String transactionCode) {
        this.transactionCode = transactionCode;
    }

    /*Define getter for transaction code*/
    @WebApiTransactionCodeDef
    public String getTransactionCode() {
        return transactionCode;
    }
}
