package pg.uk.openbank.webapi.model.transaction;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manmeet on 8/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "ProprietaryBankTransactionCode": {
 "Code": "Transfer",
 "Issuer": "AlphaBank"
 }
 */

public class ProprietaryBankTransactionCode implements Serializable {
    @SerializedName("Code")
    String code;
    @SerializedName("Issuer")
    String issuer;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
