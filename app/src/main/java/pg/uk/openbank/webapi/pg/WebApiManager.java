package pg.uk.openbank.webapi.pg;

import android.app.Activity;
import android.base.http.Builder;
import android.base.http.WebConnect;
import android.base.http.WebHandler;
import android.base.http.WebParam;
import android.base.util.ApplicationUtils;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.util.LinkedHashMap;
import java.util.Map;

import pg.uk.openbank.config.OpenBank;
import pg.uk.openbank.webapi.descriptor.GrantTypeDescriptor;
import pg.uk.openbank.webapi.descriptor.ScopeDescriptor;
import pg.uk.openbank.webapi.handler.WebApiController;
import pg.uk.openbank.webapi.handler.WebApiControllerModule;
import pg.uk.openbank.webapi.handler.error.WebApiErrorModel;
import pg.uk.openbank.webapi.model.AuthToken;
import pg.uk.openbank.webapi.model.account.AccountRequestApiRequestModel;
import pg.uk.openbank.webapi.model.account.AccountRequestApiResponseModel;
import pg.uk.openbank.webapi.model.account.AccountsApiResponseModel;
import pg.uk.openbank.webapi.model.balance.BalancesApiResponseModel;
import pg.uk.openbank.webapi.model.beneficiary.BeneficiariesApiResponseModel;
import pg.uk.openbank.webapi.model.transaction.TransactionsApiResponseModel;
import retrofit2.Response;

/**
 * Created by android on 7/8/17.
 * <p> all the api related call for the payment module will be initiated & initially handled here</p>
 */

public class WebApiManager extends WebApiControllerModule {

    /**
     * Constructor definition
     *
     * @param activity Activity
     */
    public WebApiManager(Activity activity) {
        setActivity(activity);
    }

    /**
     * Functionality to call web api to get accounts
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccountsWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            getAccountsWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                    WebApiManagerHelper.getAccessTokenForWebApiRequest(getActivity()),
                    WebApiManagerHelper.getFinancialObId(), isDialog);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccountsWebCall() -> activity object is null");
        }
    }


    /**
     * Functionality to call web api to get accounts
     *
     * @param responseCallback WebHandler.WebCallback
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getAccountsWebCall(WebHandler.OnWebCallback responseCallback,
                                   @NonNull String accessToken, boolean isDialog) {
        getAccountsWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                accessToken, WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get accounts
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          Web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getAccountsWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                   @NonNull String accessToken, boolean isDialog) {
        getAccountsWebCall(responseCallback, baseUrl, accessToken, WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get accounts
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          Web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param financialObId    financial id issues by OB to PSP
     * @param isDialog         true if need to display dialog
     */
    public void getAccountsWebCall(WebHandler.OnWebCallback responseCallback,
                                   @NonNull String baseUrl, @NonNull String accessToken,
                                   @NonNull String financialObId, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstant.GET_ACCOUNTS_CODE);

            /*Header params*/
            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, accessToken);
            //add financial id in header
            headerParams.put(WebApiConstant.FAPI_FINANCIAL_ID_PARAM, financialObId);

            /*Request params*/
            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebApiConstant.GET_ACCOUNTS)
                    .callback(this, AccountsApiResponseModel.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .headerParam(headerParams)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutor.GET_ACCOUNTS, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccountsWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get account's balances
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getBalancesWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            getBalancesWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                    WebApiManagerHelper.getAccessTokenForWebApiRequest(getActivity()),
                    WebApiManagerHelper.getFinancialObId(), isDialog);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getBalanceWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts balance
     *
     * @param responseCallback WebHandler.WebCallback
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getBalancesWebCall(WebHandler.OnWebCallback responseCallback,
                                   @NonNull String accessToken, boolean isDialog) {
        getBalancesWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(), accessToken,
                WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get accounts balance
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getBalancesWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                   @NonNull String accessToken, boolean isDialog) {
        getBalancesWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                accessToken, WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get accounts balance
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param financialObId    financial id issues by OB to PSP
     * @param isDialog         true if need to display dialog
     */
    public void getBalancesWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                   @NonNull String accessToken, @NonNull String financialObId,
                                   boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstant.GET_BALANCES_CODE);

            /*Header params*/
            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, accessToken);
            //add financial id in header
            headerParams.put(WebApiConstant.FAPI_FINANCIAL_ID_PARAM, financialObId);

            /*Request params*/
            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebApiConstant.GET_BALANCES)
                    .callback(this, BalancesApiResponseModel.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .headerParam(headerParams)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutor.GET_BALANCES, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getBalanceWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts payment transactions history
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getTransactionsWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            getTransactionsWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                    WebApiManagerHelper.getAccessTokenForWebApiRequest(getActivity()),
                    WebApiManagerHelper.getFinancialObId(),
                    isDialog);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPaymentHistoryWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts payment transactions history
     *
     * @param responseCallback WebHandler.WebCallback
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getTransactionsWebCall(WebHandler.OnWebCallback responseCallback,
                                       @NonNull String accessToken, boolean isDialog) {
        getTransactionsWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                accessToken,
                WebApiManagerHelper.getFinancialObId(),
                isDialog);
    }

    /**
     * Functionality to call web api to get accounts payment transactions history
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getTransactionsWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                       @NonNull String accessToken, boolean isDialog) {
        getTransactionsWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                accessToken,
                WebApiManagerHelper.getFinancialObId(),
                isDialog);
    }

    /**
     * Functionality to call web api to get accounts payment history
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          Web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param financialObId    financial id issues by OB to PSP
     * @param isDialog         true if need to display dialog
     */
    public void getTransactionsWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                       @NonNull String accessToken, @NonNull String financialObId,
                                       boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstant.GET_TRANSACTIONS_CODE);

            /*Header params*/
            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, accessToken);
            //add financial id in header
            headerParams.put(WebApiConstant.FAPI_FINANCIAL_ID_PARAM, financialObId);

            /*Request params*/
            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebApiConstant.GET_TRANSACTIONS)
                    .callback(this, TransactionsApiResponseModel.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .headerParam(headerParams)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutor.GET_TRANSACTIONS, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPaymentHistoryWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts already added beneficiaries
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getBeneficiariesWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            getBeneficiariesWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(),
                    WebApiManagerHelper.getAccessTokenForWebApiRequest(getActivity()),
                    WebApiManagerHelper.getFinancialObId(), isDialog);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPayeesWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts already added beneficiaries
     *
     * @param responseCallback WebHandler.WebCallback
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getBeneficiariesWebCall(WebHandler.OnWebCallback responseCallback,
                                        @NonNull String accessToken, boolean isDialog) {
        getBeneficiariesWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(), accessToken,
                WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get accounts already added beneficiaries
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          Web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getBeneficiariesWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                        @NonNull String accessToken, boolean isDialog) {
        getBeneficiariesWebCall(responseCallback, baseUrl,
                accessToken, WebApiManagerHelper.getFinancialObId(), isDialog);
    }


    /**
     * Functionality to call web api to get accounts already added beneficiaries
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          web api url
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param financialObId    financial id issues by OB to PSP
     * @param isDialog         true if need to display dialog
     */
    public void getBeneficiariesWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                        @NonNull String accessToken, @NonNull String financialObId,
                                        boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstant.GET_BENEFICIARIES_CODE);

            /*Header params*/
            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, accessToken);
            //add financial id in header
            headerParams.put(WebApiConstant.FAPI_FINANCIAL_ID_PARAM, financialObId);

            /*Request params*/
            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebApiConstant.GET_BENEFICIARIES)
                    .callback(this, BeneficiariesApiResponseModel.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .headerParam(headerParams)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutor.GET_BENEFICIARIES, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPayeesWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get access token
     *
     * @param callbackUri      callback uri coming from authentication
     * @param scope            @ScopeDescriptor.WebApiScopeDef.ACCOUNTS/PAYMENTS based on AISP/PISP
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccessTokenWebCall(@NonNull Uri callbackUri, @ScopeDescriptor.WebApiScopeDef String scope,
                                      WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        getAccessTokenWebCall(WebApiManagerHelper.getResolvedWebApiUrl(), callbackUri, scope, null, null, responseCallback, isDialog);
    }

    /**
     * Functionality to call web api to get access token
     *
     * @param scope            @ScopeDescriptor.WebApiScopeDef.ACCOUNTS/PAYMENTS based on AISP/PISP
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccessTokenWebCall(@ScopeDescriptor.WebApiScopeDef String scope,
                                      WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        getAccessTokenWebCall(WebApiManagerHelper.getResolvedWebApiUrl(), Uri.parse(OpenBank.getRedirectUriString()),
                scope, null, null, responseCallback, isDialog);
    }

    /**
     * Functionality to call web api to get access token
     *
     * @param baseUrl          web api url
     * @param scope            @ScopeDescriptor.WebApiScopeDef.ACCOUNTS/PAYMENTS based on AISP/PISP
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccessTokenWebCall(@NonNull String baseUrl, @ScopeDescriptor.WebApiScopeDef String scope,
                                      WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        getAccessTokenWebCall(baseUrl, Uri.parse(OpenBank.getRedirectUriString()),
                scope, null, null, responseCallback, isDialog);
    }

    /**
     * Functionality to call web api to get access token
     *
     * @param baseUrl          Web api url
     * @param callbackUri      callback uri coming from authentication
     * @param scope            @ScopeDescriptor.WebApiScopeDef.ACCOUNTS/PAYMENTS based on AISP/PISP
     * @param assertion
     * @param assertionType
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccessTokenWebCall(@NonNull String baseUrl, @NonNull Uri callbackUri, @ScopeDescriptor.WebApiScopeDef String scope,
                                      @Nullable String assertion, @Nullable String assertionType,
                                      WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstant.ACCESS_TOKEN_CODE);

             /*Header params*/
            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, WebApiManagerHelper.getTokenWebApiAuthorizationHeaderValue());

            /*Request parameters*/
            Map<String, Object> params = new LinkedHashMap<>();
            params.put(WebApiConstant.CLIENT_ID_PARAM, OpenBank.getClientId());
            params.put(WebApiConstant.SCOPE_PARAM, scope);
            params.put(WebApiConstant.GRANT_TYPE_PARAM, GrantTypeDescriptor.WebApiGrantTypeDef.CLIENT_CREDENTIALS);
            params.put(WebApiConstant.REDIRECT_URI_PARAM, callbackUri.toString());
            params.put(WebApiConstant.CLIENT_ASSERTION_PARAM,
                    ApplicationUtils.Validator.isEmptyOrNull(assertion)
                            ? "" : assertion);
            params.put(WebApiConstant.CLIENT_ASSERTION_TYPE_PARAM,
                    ApplicationUtils.Validator.isEmptyOrNull(assertionType)
                            ? "" : assertionType);

            Builder builder = WebConnect.with(getActivity(), WebApiConstant.ACCESS_TOKEN)
                    .callback(this, AuthToken.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.POST)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .headerParam(headerParams)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutor.CREATE_ACCESS_TOKEN, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccessTokenWebCall() -> activity object is null");
        }
    }


    /**
     * Functionality to call web api to get account request token
     *
     * @param responseCallback WebHandler.WebCallback
     * @param payload          {@link AccountRequestApiRequestModel}
     * @param isDialog         true if need to display dialog
     */
    public void getAccountRequestTokenWebCall(WebHandler.OnWebCallback responseCallback,
                                              AccountRequestApiRequestModel payload,
                                              boolean isDialog) {
        if (getActivity() != null) {
            getAccountRequestTokenWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(), payload,
                    WebApiManagerHelper.getAccessTokenForWebApiRequest(getActivity()),
                    WebApiManagerHelper.getFinancialObId(), isDialog);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccountRequestTokenWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get account request token
     *
     * @param responseCallback WebHandler.WebCallback
     * @param payload          {@link AccountRequestApiRequestModel}
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getAccountRequestTokenWebCall(WebHandler.OnWebCallback responseCallback,
                                              AccountRequestApiRequestModel payload,
                                              @NonNull String accessToken, boolean isDialog) {
        getAccountRequestTokenWebCall(responseCallback, WebApiManagerHelper.getResolvedWebApiUrl(), payload, accessToken,
                WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get account request token
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          web api url
     * @param payload          {@link AccountRequestApiRequestModel}
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param isDialog         true if need to display dialog
     */
    public void getAccountRequestTokenWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                              AccountRequestApiRequestModel payload,
                                              @NonNull String accessToken, boolean isDialog) {
        getAccountRequestTokenWebCall(responseCallback, baseUrl, payload, accessToken,
                WebApiManagerHelper.getFinancialObId(), isDialog);
    }

    /**
     * Functionality to call web api to get account request token
     *
     * @param responseCallback WebHandler.WebCallback
     * @param baseUrl          web api url
     * @param payload          {@link AccountRequestApiRequestModel}
     * @param accessToken      access token of logged-in user  example = "Bearer 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5"
     * @param financialObId    financial id issues by OB to PSP
     * @param isDialog         true if need to display dialog
     */
    public void getAccountRequestTokenWebCall(WebHandler.OnWebCallback responseCallback, @NonNull String baseUrl,
                                              AccountRequestApiRequestModel payload, @NonNull String accessToken,
                                              @NonNull String financialObId, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebApiConstant.ACCOUNT_REQUEST_CODE);


            Map<String, String> headerParams = new LinkedHashMap<>();
            //add access token in header
            headerParams.put(WebApiConstant.AUTHORIZATION_KEY, accessToken);
            //add financial id in header
            headerParams.put(WebApiConstant.FAPI_FINANCIAL_ID_PARAM, financialObId);
            //add jws token in header
            headerParams.put(WebApiConstant.JWS_SIGNATURE_PARAM, WebApiManagerHelper.createJwsToken(new Gson().toJson(payload)));

            Map<String, Object> params = new LinkedHashMap<>();
            params.put("", payload);


            Builder builder = WebConnect.with(getActivity(), WebApiConstant.ACCOUNT_REQUEST)
                    .callback(this, AccountRequestApiResponseModel.class, WebApiErrorModel.class)
                    .httpType(WebParam.HttpType.POST)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .baseUrl(baseUrl)
                    .taskId(getTaskId());

            // WebCall
            WebApiController.apiCall(WebApiExecutor.ACCOUNT_REQUEST, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccountRequestTokenWebCall() -> activity object is null");
        }
    }

    /*****************************
     * Web server response functionality
     ****************************/
    @Override
    public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
        super.onSuccess(object, taskId, response);
        if (getCallingScreenWebApiResponseCallback() != null) {
            getCallingScreenWebApiResponseCallback().onSuccess(object, taskId, response);
        }
    }

    @Override
    public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
        super.onError(object, error, taskId, response);
        if (getCallingScreenWebApiResponseCallback() != null) {
            getCallingScreenWebApiResponseCallback().onError(object, error, taskId, response);
        }
    }
}
