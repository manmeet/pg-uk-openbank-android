package pg.uk.openbank.webapi.pg.sampleapp;

import android.base.http.Builder;
import android.base.http.RetrofitManager;

import java.util.Map;

import pg.uk.openbank.webapi.handler.WebApiInterface;
import pg.uk.openbank.webapi.model.AuthToken;
import pg.uk.openbank.webapi.pg.WebApi;
import pg.uk.openbank.webapi.pg.sampleapp.model.LoginResponse;

/**
 * Created by manmeet on 14/3/18.
 * <p>To call web apis based on sample app (Android sample application) code provided by Apigee </p>
 */

public enum WebApiExecutorSampleApp implements WebApiInterface {

    //create and get access token
    SAMPLE_APP_LOGIN_API() {
        @Override
        public <T> T execute(Builder builder) {
            builder.connect(WebApiSampleApp.class).sampleAppLogin((Map<String, Object>) builder.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<LoginResponse>(builder.getWebParam()));
            return null;
        }
    }
}
