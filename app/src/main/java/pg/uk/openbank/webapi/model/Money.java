package pg.uk.openbank.webapi.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * Created by manmeet on 8/3/18.
 * "Amount": {
 * "Amount": "1230.00",
 * "Currency": "GBP"
 * }
 */

public class Money implements Serializable {
    @SerializedName("Amount")
    String amount;
    @SerializedName("Currency")
    String currency;

    /***************
     *Getters & Setters*
     **************/
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
