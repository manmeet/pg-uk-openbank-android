package pg.uk.openbank.webapi.model.account;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "  Data": {
 * "Account": [
 * {
 * "AccountId": "22289",
 * "Currency": "GBP",
 * "Nickname": "Bills"
 * }
 * ]
 * },
 * "Links": {
 * "Self": "/accounts/"
 * },
 * "MetaData": {
 * "TotalPages": 1
 * }
 * }
 */

public class Account implements Serializable {
    //basic info
    @SerializedName("AccountId")
    String id;
    @SerializedName("Nickname")
    String name;
    @SerializedName("number")
    String number;
    @SerializedName("accountNumber")
    String accountNumber;
    @SerializedName("sortCode")
    String sortCode;
    @SerializedName("Currency")
    String currency;
    @SerializedName("iban")
    String iban;
    @SerializedName("bic")
    String bic;


    /***************
     *Getters & Setters*
     **************/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
