package pg.uk.openbank.webapi.pg.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class LoginResponse {
    @SerializedName("links")
    @Expose
    private List<Link> links = new ArrayList();

    public List<Link> getLinks() {
        return this.links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
