package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manmeet on 8/3/18.
 * <p>Contains the scope of api which is defined to access web api data based on AISP/PISP</p>
 */

public class ScopeDescriptor {

    // Enumerate valid values for this interface
    @StringDef({WebApiScopeDef.ACCOUNTS, WebApiScopeDef.PAYMENTS, WebApiScopeDef.OPEN_ID})
    @Retention(RetentionPolicy.SOURCE)
    // Describes when the annotation will be discarded
    public @interface WebApiScopeDef { // Create an interface for validating String types
        String ACCOUNTS = "accounts";
        String PAYMENTS = "payment";
        String OPEN_ID = "openid";
    }

    /*Define variable for scope*/
    @WebApiScopeDef
    String scope;

    /*Define setter for scope*/
    public void setScope(@WebApiScopeDef String scope) {
        this.scope = scope;
    }

    /*Define getter for scope*/
    @WebApiScopeDef
    public String getScope() {
        return scope;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(WebApiScopeDef.OPEN_ID);
        builder.append(" ");
        builder.append(WebApiScopeDef.ACCOUNTS);
        builder.append(" ");
        builder.append(WebApiScopeDef.PAYMENTS);
        return builder.toString();
    }
}
