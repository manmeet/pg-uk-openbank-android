package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manmeet on 8/3/18.
 * <p>Contains the credit line types Pre-Agreed/else</p>
 */

public class CreditLineTypeDescriptor {

    // Enumerate valid values for this interface
    @StringDef({CreditLineTypeDef.PRE_AGREED})
    @Retention(RetentionPolicy.SOURCE)
    // Describes when the annotation will be discarded
    public @interface CreditLineTypeDef { // Create an interface for validating String types
        String PRE_AGREED = "Pre-Agreed";
    }

    /*Define variable for credit line type*/
    @CreditLineTypeDef
    String creditLineType;

    /*Define setter for credit line type*/
    public void setCreditLineType(@CreditLineTypeDef String creditLineType) {
        this.creditLineType = creditLineType;
    }

    /*Define getter for credit line type*/
    @CreditLineTypeDef
    public String getCreditLineType() {
        return creditLineType;
    }
}
