package pg.uk.openbank.webapi.pg;

import java.util.Map;

import pg.uk.openbank.webapi.model.AuthToken;
import pg.uk.openbank.webapi.model.account.AccountRequestApiResponseModel;
import pg.uk.openbank.webapi.model.account.AccountsApiResponseModel;
import pg.uk.openbank.webapi.model.balance.BalancesApiResponseModel;
import pg.uk.openbank.webapi.model.beneficiary.BeneficiariesApiResponseModel;
import pg.uk.openbank.webapi.model.transaction.TransactionsApiResponseModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * <p>Contains the interface methods for all payment related apis</p>
 */

public interface WebApi {
    @GET(WebApiConstant.GET_ACCOUNTS)
    Call<AccountsApiResponseModel> getAccounts(@QueryMap Map<String, Object> requestBody);

    @GET(WebApiConstant.GET_BALANCES)
    Call<BalancesApiResponseModel> getBalances(@QueryMap Map<String, Object> requestBody);

    @GET(WebApiConstant.GET_BENEFICIARIES)
    Call<BeneficiariesApiResponseModel> getBeneficiaries(@QueryMap Map<String, Object> requestBody);

    @GET(WebApiConstant.GET_TRANSACTIONS)
    Call<TransactionsApiResponseModel> getTransactions(@QueryMap Map<String, Object> requestBody);

    @FormUrlEncoded
    @POST(WebApiConstant.ACCESS_TOKEN)
    Call<AuthToken> createAccessToken(@FieldMap Map<String, Object> requestBody);

    @POST(WebApiConstant.ACCOUNT_REQUEST)
    Call<AccountRequestApiResponseModel> createAccountRequest(@Body Map<String, Object> requestBody);

//    @GET(WebConstant.GET_PAYEE_DETAIL)
//    Call<PayeeDetailResponseModel> getPayeeDetail(@Path(WebConstant.ID_PARAM) String contactId,
//                                                  @QueryMap Map<String, Object> requestBody);
}
