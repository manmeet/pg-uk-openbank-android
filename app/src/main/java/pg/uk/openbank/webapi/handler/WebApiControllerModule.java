package pg.uk.openbank.webapi.handler;

import android.app.Activity;
import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import retrofit2.Response;

/**
 * Created by android on 20/9/16.
 * <p>used as a base controller class for web api request and response from module's screens</p>
 * @see pg.uk.openbank.webapi.pg.WebApiManager
 */

public class WebApiControllerModule implements WebHandler.OnWebCallback {
    /*contains the call back of web api response of calling screen if required for future purposes*/
    private WebHandler.OnWebCallback callingScreenWebApiResponseCallback;
    /**
     * contains the activity object of calling screen
     */
    private Activity activity;
    /**
     * web task identifier of calling screen
     */
    private int taskId;


    /*****************
     * Getters & Setters*
     **************/
    public WebHandler.OnWebCallback getCallingScreenWebApiResponseCallback() {
        return callingScreenWebApiResponseCallback;
    }

    public void setCallingScreenWebApiResponseCallback(WebHandler.OnWebCallback callingScreenWebApiResponseCallback) {
        this.callingScreenWebApiResponseCallback = callingScreenWebApiResponseCallback;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Override
    public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
        //just an implementation
    }

    @Override
    public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
        //just an implementation
    }
}
