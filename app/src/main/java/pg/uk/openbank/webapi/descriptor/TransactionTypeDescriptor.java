package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manmeet on 8/3/18.
 * <p>Contains the transaction types debit/credit</p>
 */

public class TransactionTypeDescriptor {

    // Enumerate valid values for this interface
    @StringDef({WebApiTransactionTypeDef.DEBIT, WebApiTransactionTypeDef.CREDIT})
    @Retention(RetentionPolicy.SOURCE)
    // Describes when the annotation will be discarded
    public @interface WebApiTransactionTypeDef { // Create an interface for validating String types
        String DEBIT = "debit";
        String CREDIT = "credit";
    }

    /*Define variable for transaction type*/
    @WebApiTransactionTypeDef
    String transactionType;

    /*Define setter for transaction type*/
    public void setTransactionType(@WebApiTransactionTypeDef String transactionType) {
        this.transactionType = transactionType;
    }

    /*Define getter for transaction type*/
    @WebApiTransactionTypeDef
    public String getTransactionType() {
        return transactionType;
    }
}
