package pg.uk.openbank.webapi.model;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import pg.uk.openbank.webapi.handler.error.WebApiErrorConstant;

/**
 * Created by manmeet on 8/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "Meta": {
 * "TotalPages": 20,
 * "FirstAvailableDateTime": "2017-05-03T00:00:00+00:00",
 * "LastAvailableDateTime": "2017-12-03T00:00:00+00:00"
 * }
 */

public class MetaData implements Serializable {
    @SerializedName("TotalPages")
    String totalPages;
    @SerializedName("FirstAvailableDateTime")
    String firstAvailableDateTime;
    @SerializedName("LastAvailableDateTime")
    String lastAvailableDateTime;

    /***************
     *Getters & Setters*
     **************/

    public int getTotalPages() {
        Optional<String> value = Optional.of(totalPages);
        return value.isPresent() ? Integer.parseInt(totalPages) : WebApiErrorConstant.BLANK_INT;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public String getFirstAvailableDateTime() {
        return firstAvailableDateTime;
    }

    public void setFirstAvailableDateTime(String firstAvailableDateTime) {
        this.firstAvailableDateTime = firstAvailableDateTime;
    }

    public String getLastAvailableDateTime() {
        return lastAvailableDateTime;
    }

    public void setLastAvailableDateTime(String lastAvailableDateTime) {
        this.lastAvailableDateTime = lastAvailableDateTime;
    }
}
