package pg.uk.openbank.webapi.model.beneficiary;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pg.uk.openbank.webapi.model.Links;
import pg.uk.openbank.webapi.model.MetaData;

/**
 * Created by manmeet on 9/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "Data": {
 * "Beneficiary": [
 * {
 * "AccountId": "22289",
 * "BeneficiaryId": "Ben1",
 * "Reference": "Towbar Club",
 * "CreditorAccount": {
 * "SchemeName": "SortCodeAccountNumber",
 * "Identification": "80200112345678",
 * "Name": "Mrs Juniper"
 * }
 * }
 * ]
 * },
 * "Links": {
 * "Self": "/accounts/22289/beneficiaries/"
 * },
 * "Meta": {
 * "TotalPages": 1
 * }
 * }
 */

public class BeneficiariesApiResponseModel implements Serializable {
    //basic info
    @SerializedName("Data")
    private BeneficiariesData beneficiariesData;

    //other info
    @SerializedName("Links")
    private Links links;
    @SerializedName("Meta")
    private MetaData metaData;

    public class BeneficiariesData implements Serializable{
        @SerializedName("Beneficiary")
        private List<Beneficiary> beneficiaries;

        /***************
         *Getters & Setters*
         **************/
        public List<Beneficiary> getBeneficiaries() {
            return Optional.fromNullable(beneficiaries).or(new ArrayList<Beneficiary>());
        }

        public void setBeneficiaries(List<Beneficiary> beneficiaries) {
            this.beneficiaries = beneficiaries;
        }
    }

    /***************
     *Getters & Setters*
     **************/

    public BeneficiariesData getBeneficiariesData() {
        return beneficiariesData;
    }

    public void setBeneficiariesData(BeneficiariesData beneficiariesData) {
        this.beneficiariesData = beneficiariesData;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
