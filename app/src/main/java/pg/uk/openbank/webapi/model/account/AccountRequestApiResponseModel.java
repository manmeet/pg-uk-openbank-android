package pg.uk.openbank.webapi.model.account;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import pg.uk.openbank.webapi.model.Links;
import pg.uk.openbank.webapi.model.MetaData;

/**
 * Created by manmeet on 9/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "Data": {
 * "AccountRequestId": "88379",
 * "Status": "AwaitingAuthorisation",
 * "CreationDateTime": "2017-05-02T00:00:00+00:00",
 * "Permissions": [
 * "ReadAccountsDetail",
 * "ReadBalances",
 * "ReadBeneficiariesDetail",
 * "ReadDirectDebits",
 * "ReadProducts",
 * "ReadStandingOrdersDetail",
 * "ReadTransactionsCredits",
 * "ReadTransactionsDebits",
 * "ReadTransactionsDetail"
 * ],
 * "ExpirationDateTime": "2017-08-02T00:00:00+00:00",
 * "TransactionFromDateTime": "2017-05-03T00:00:00+00:00",
 * "TransactionToDateTime": "2017-12-03T00:00:00+00:00"
 * },
 * "Risk": {},
 * "Links": {
 * "Self": "/account-requests/88379"
 * },
 * "Meta": {
 * "TotalPages": 1
 * }
 * }
 */

public class AccountRequestApiResponseModel implements Serializable {
    //basic info
    @SerializedName("Data")
    private AccountRequestData accountRequestData;

    //other info
    @SerializedName("Links")
    private Links links;
    @SerializedName("Meta")
    private MetaData metaData;

    public class AccountRequestData implements Serializable{
        @SerializedName("AccountRequestId")
        private String accountRequestId;
        @SerializedName("Status")
        private String status;
        @SerializedName("CreationDateTime")
        private String creationDateTime;
        @SerializedName("Permissions")
        private String[] permissions;

        /***************
         *Getters & Setters*
         **************/
        public String getAccountRequestId() {
            return accountRequestId;
        }

        public void setAccountRequestId(String accountRequestId) {
            this.accountRequestId = accountRequestId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreationDateTime() {
            return creationDateTime;
        }

        public void setCreationDateTime(String creationDateTime) {
            this.creationDateTime = creationDateTime;
        }

        public String[] getPermissions() {
            return permissions;
        }

        public void setPermissions(String[] permissions) {
            this.permissions = permissions;
        }
    }

    /***************
     *Getters & Setters*
     **************/

    public AccountRequestData getAccountRequestData() {
        return accountRequestData;
    }

    public void setAccountRequestData(AccountRequestData accountRequestData) {
        this.accountRequestData = accountRequestData;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
