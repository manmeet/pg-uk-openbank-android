package pg.uk.openbank.webapi.pg.sampleapp;

import java.util.Map;

import pg.uk.openbank.webapi.pg.sampleapp.model.LoginResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by manmeet on 14/3/18.
 * <p>web api methods based on sample app (Android sample application) code provided by Apigee </p>
 */

public interface WebApiSampleApp {
    @GET(WebApiConstantSampleApp.SAMPLE_APP_LOGIN_API_METHOD)
    Call<LoginResponse> sampleAppLogin(@QueryMap Map<String, Object> requestBody);
}
