package pg.uk.openbank.webapi.handler.error;

/**
 */

public class WebApiErrorConstant {
    /*private constructor because still all the members of this class are of class level*/
    private WebApiErrorConstant(){}

    /*default value of int for the application*/
    public static final int BLANK_INT = 0;
    /*default value of string for the application*/
    public static final String BLANK_STRING = "";
}
