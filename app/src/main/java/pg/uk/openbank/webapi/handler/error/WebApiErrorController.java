package pg.uk.openbank.webapi.handler.error;

import android.app.Activity;
import android.base.alert.Alert;
import android.base.util.ApplicationUtils;
import android.support.design.widget.Snackbar;

import org.json.JSONArray;
import org.json.JSONTokener;

import java.net.HttpURLConnection;

import pg.uk.openbank.R;


/**
 * This class is used to validate all the errors coming from server web apis
 */
public class WebApiErrorController {
    /**
     * Functionality to check error codes that are coming from server web apis
     * and do task accordingly
     *
     * @param context    Activity object
     * @param dataModel  generic type class
     * @param errorMsg   message
     * @param taskId     web call task id #webparam
     * @param statusCode http code
     */
    public static <T> String resolveErrorCodes(Activity context, T dataModel,
                                                String errorMsg, int taskId,
                                                int statusCode) {
        //here we can make a check globally for the application
        // such as token expire or just display message directly

        //check for string array error
        try {
            //check if error response is not blank string or null
            if (!ApplicationUtils.Validator.isEmptyOrNull(errorMsg)) {
                Object responseModel = new JSONTokener(errorMsg).nextValue();
                //now check if error response is simple string array
                if (responseModel instanceof JSONArray) {
                    //handle string array response
//                    List<String> errors = new Gson().fromJson(errorMsg,
//                            new TypeToken<ArrayList<String>>() {
//                            }.getType());
//                    if (errors != null
//                            && !errors.isEmpty()) {
//                        errorMsg = context.getResources().getString(WebApiErrorCode.valueOf(errors.get(0)).getValue());
//                        showErrorMsg(context, dataModel, errorMsg, taskId, statusCode);
//                    }
                }
                //otherwise check if object model is of #WebApiErrorModel
                else if (dataModel instanceof WebApiErrorModel) {
                    WebApiErrorModel errorModel = (WebApiErrorModel) dataModel;
                    //check for expire token
//                    if (errorModel.getMessage().equalsIgnoreCase(WebApiErrorCode.invalid_token.name())) {
//                        //access token has expired
//                        //TODO:// authorise here again
//                        ApplicationUtils.Log.d("resolveErrorCodes() -> access toke expired = "+errorModel.getMessage());
//                        errorMsg = context.getResources().getString(WebApiErrorCode.valueOf(errorModel.getMessage()).getValue());
//                        showMessage(context, errorMsg, taskId);
//                    } else {
//                        errorMsg = context.getResources().getString(WebApiErrorCode.valueOf(errorModel.getMessage()).getValue());
//                        showErrorMsg(context, dataModel, errorMsg, taskId, statusCode);
//                    }
                }
            }
        } catch (Exception e) {
            ApplicationUtils.Log.e("resolveErrorCodes() -> " + e.getMessage());
        }

        return errorMsg;
    }

    /**
     * Functionality to show error message from server web api response
     *
     * @param context    Activity object
     * @param dataModel  generic type class
     * @param errorMsg   message
     * @param statusCode http code
     */
    public static <T> void showErrorMsg(Activity context, T dataModel,
                                        String errorMsg, int taskId, int statusCode) {
        try {
            //find error message that need to display
            String messageToDisplay = "";
            if (dataModel instanceof WebApiErrorModel
                    && context != null) {
                WebApiErrorModel errorModel = (WebApiErrorModel) dataModel;
                if (!ApplicationUtils.Validator.isEmptyOrNull(errorModel.getMessage())) {
                    messageToDisplay = errorModel.getMessage();
                } else if (!ApplicationUtils.Validator.isEmptyOrNull(errorMsg)) {
                    messageToDisplay = errorMsg;
                }
            } else if (!ApplicationUtils.Validator.isEmptyOrNull(errorMsg)) {
                messageToDisplay = errorMsg;
            }

            //now validate error message
            //check for error message & internet error
            if (!ApplicationUtils.Validator.isEmptyOrNull(messageToDisplay)
                    && statusCode == HttpURLConnection.HTTP_GONE) {
                showMessage(context, messageToDisplay, taskId, Snackbar.LENGTH_INDEFINITE);
            }
            //check for only error message
            else if (!ApplicationUtils.Validator.isEmptyOrNull(messageToDisplay)) {
                showMessage(context, messageToDisplay, taskId);
            }

        } catch (Exception e) {
            ApplicationUtils.Log.e(e.getMessage() + e);
        }

    }

    /**
     * Functionality to display error message popup/snackbar/toast
     *
     * @param activity Activity object
     * @param errorMsg message
     * @param taskId   task identifier
     */
    private static void showMessage(Activity activity, String errorMsg, int taskId) {
        showMessage(activity, errorMsg, taskId, Snackbar.LENGTH_LONG);
    }

    /**
     * Functionality to display error message popup/snackbar/toast
     *
     * @param activity Activity object
     * @param errorMsg message
     * @param taskId   task identifier
     * @param duration {@link Snackbar}
     */
    private static void showMessage(Activity activity, String errorMsg, int taskId, int duration) {
        Alert.with(activity, errorMsg, R.color.colorPrimaryDark)
                .actionMessage(android.R.string.ok)
                .textColor(R.color.white)
                .actionColor(R.color.white)
                .uniqueId(taskId)
                .duration(duration)
                .show();
    }


}
