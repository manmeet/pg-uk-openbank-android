package pg.uk.openbank.webapi.model.balance;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pg.uk.openbank.webapi.model.Money;
import pg.uk.openbank.webapi.model.CreditLine;

/**
 * Created by manmeet on 8/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "Data": {
 * "Balance": [
 * {
 * "AccountId": "22289",
 * "Amount": {
 * "Amount": "1230.00",
 * "Currency": "GBP"
 * },
 * "CreditDebitIndicator": "Credit",
 * "Type": "InterimAvailable",
 * "DateTime": "2017-04-05T10:43:07+00:00",
 * "CreditLine": [
 * {
 * "Included": true,
 * "Amount": {
 * "Amount": "1000.00",
 * "Currency": "GBP"
 * },
 * "Type": "Pre-Agreed"
 * }
 * ]
 * }
 * ]
 * },
 * "Links": {
 * "Self": "/accounts/22289/balances/"
 * },
 * "Meta": {
 * "TotalPages": 1
 * }
 * }
 */

public class Balance implements Serializable {
    @SerializedName("AccountId")
    private String accountId;
    @SerializedName("Amount")
    private Money amount;
    @SerializedName("CreditDebitIndicator")
    private String creditDebitIndicator;
    @SerializedName("Type")
    private String type;
    @SerializedName("DateTime")
    private String dateTime;
    //@SerializedName("CreditLine")
    private List<CreditLine> creditLines;

   //TODO: remove this because this is the object in Apigee but array in Openbanking format
    @SerializedName("CreditLine")
    private CreditLine creditLine;

    /***************
     *Getters & Setters*
     **************/
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Money getAmount() {
        return amount;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }

    public String getCreditDebitIndicator() {
        return creditDebitIndicator;
    }

    public void setCreditDebitIndicator(String creditDebitIndicator) {
        this.creditDebitIndicator = creditDebitIndicator;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public List<CreditLine> getCreditLines() {
        return Optional.fromNullable(creditLines).or(new ArrayList<CreditLine>());
    }

    public void setCreditLines(List<CreditLine> creditLines) {
        this.creditLines = creditLines;
    }
}
