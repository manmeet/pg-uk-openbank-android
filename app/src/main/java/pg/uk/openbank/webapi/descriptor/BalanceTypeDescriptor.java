package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manmeet on 8/3/18.
 * <p>Contains the balance types InterimAvailable/else</p>
 */

public class BalanceTypeDescriptor {

    // Enumerate valid values for this interface
    @StringDef({BalanceTypeDef.INTERIM_AVAILABLE})
    @Retention(RetentionPolicy.SOURCE)
    // Describes when the annotation will be discarded
    public @interface BalanceTypeDef { // Create an interface for validating String types
        String INTERIM_AVAILABLE = "InterimAvailable";
    }

    /*Define variable for balance type*/
    @BalanceTypeDef
    String balanceType;

    /*Define setter for balance type*/
    public void setBalanceType(@BalanceTypeDef String balanceType) {
        this.balanceType = balanceType;
    }

    /*Define getter for balance type*/
    @BalanceTypeDef
    public String getBalanceType() {
        return balanceType;
    }
}
