package pg.uk.openbank.webapi.model.transaction;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import pg.uk.openbank.webapi.model.Money;
import pg.uk.openbank.webapi.model.balance.Balance;

/**
 * Created by manmeet on 8/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * {
 * "Data": {
 * "Transaction": [
 * {
 * "AccountId": "22289",
 * "TransactionId": "123",
 * "TransactionReference": "Ref 1",
 * "Amount": {
 * "Amount": "10.00",
 * "Currency": "GBP"
 * },
 * "CreditDebitIndicator": "Credit",
 * "Status": "Booked",
 * "BookingDateTime": "2017-04-05T10:43:07+00:00",
 * "ValueDateTime": "2017-04-05T10:45:22+00:00",
 * "TransactionInformation": "Cash from Aubrey",
 * "BankTransactionCode": {
 * "Code": "ReceivedCreditTransfer",
 * "SubCode": "DomesticCreditTransfer"
 * },
 * "ProprietaryBankTransactionCode": {
 * "Code": "Transfer",
 * "Issuer": "AlphaBank"
 * },
 * "Balance": {
 * "Amount": {
 * "Amount": "230.00",
 * "Currency": "GBP"
 * },
 * "CreditDebitIndicator": "Credit",
 * "Type": "InterimBooked"
 * }
 * }
 * ]
 * },
 * "Links": {
 * "Self": "/accounts/22289/transactions/"
 * },
 * "Meta": {
 * "TotalPages": 1,
 * "FirstAvailableDateTime": "2017-05-03T00:00:00+00:00",
 * "LastAvailableDateTime": "2017-12-03T00:00:00+00:00"
 * }
 * }
 */

public class Transaction implements Serializable {
    @SerializedName("AccountId")
    String accountId;
    @SerializedName("Amount")
    Money amount;
    @SerializedName("TransactionId")
    String transactionId;
    @SerializedName("TransactionReference")
    String transactionReference;
    @SerializedName("BookingDateTime")
    String bookingDateTime;
    @SerializedName("CreditDebitIndicator")
    String creditDebitIndicator;
    @SerializedName("Status")
    String status;
    @SerializedName("ValueDateTime")
    String valueDateTime;
    @SerializedName("TransactionInformation")
    String transactionInformation;
    @SerializedName("BankTransactionCode")
    BankTransactionCode bankTransactionCode;
    @SerializedName("ProprietaryBankTransactionCode")
    ProprietaryBankTransactionCode proprietaryBankTransactionCode;
    @SerializedName("Balance")
    Balance balance;

    /***************
     *Getters & Setters*
     **************/
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Money getAmount() {
        return amount;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(String bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public String getCreditDebitIndicator() {
        return creditDebitIndicator;
    }

    public void setCreditDebitIndicator(String creditDebitIndicator) {
        this.creditDebitIndicator = creditDebitIndicator;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValueDateTime() {
        return valueDateTime;
    }

    public void setValueDateTime(String valueDateTime) {
        this.valueDateTime = valueDateTime;
    }

    public String getTransactionInformation() {
        return transactionInformation;
    }

    public void setTransactionInformation(String transactionInformation) {
        this.transactionInformation = transactionInformation;
    }

    public BankTransactionCode getBankTransactionCode() {
        return bankTransactionCode;
    }

    public void setBankTransactionCode(BankTransactionCode bankTransactionCode) {
        this.bankTransactionCode = bankTransactionCode;
    }

    public ProprietaryBankTransactionCode getProprietaryBankTransactionCode() {
        return proprietaryBankTransactionCode;
    }

    public void setProprietaryBankTransactionCode(ProprietaryBankTransactionCode proprietaryBankTransactionCode) {
        this.proprietaryBankTransactionCode = proprietaryBankTransactionCode;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }
}
