package pg.uk.openbank.webapi.descriptor;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * Created by manmeet on 7/3/18.
 * <p>Contains the permissions defined to access web api data</p>
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "Permissions": [
 "ReadAccountsDetail",
 "ReadBalances",
 "ReadBeneficiariesDetail",
 "ReadDirectDebits",
 "ReadProducts",
 "ReadStandingOrdersDetail",
 "ReadTransactionsCredits",
 "ReadTransactionsDebits",
 "ReadTransactionsDetail"
 ]
 */

public class PermissionDescriptor {

    // Enumerate valid values for this interface
    @StringDef({WebApiReadPermissionDef.READ_ACCOUNTS_DETAIL, WebApiReadPermissionDef.READ_BALANCES,
            WebApiReadPermissionDef.READ_BENEFICIARIES_DETAIL,WebApiReadPermissionDef.READ_DIRECT_DEBITS,
            WebApiReadPermissionDef.READ_PRODUCTS, WebApiReadPermissionDef.READ_STANDING_ORDERS_DETAIL,
            WebApiReadPermissionDef.READ_TRANSACTIONS_CREDITS, WebApiReadPermissionDef.READ_TRANSACTIONS_DEBITS,
            WebApiReadPermissionDef.READ_TRANSACTIONS_DETAIL})
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Create an interface for validating String types
    public @interface WebApiReadPermissionDef {
        // Declare the constants for read permissions
        String READ_ACCOUNTS_DETAIL = "ReadAccountsDetail";
        String READ_BALANCES = "ReadBalances";
        String READ_BENEFICIARIES_DETAIL = "ReadBeneficiariesDetail";
        String READ_DIRECT_DEBITS = "ReadDirectDebits";
        String READ_PRODUCTS = "ReadProducts";
        String READ_STANDING_ORDERS_DETAIL = "ReadStandingOrdersDetail";
        String READ_TRANSACTIONS_CREDITS = "ReadTransactionsCredits";
        String READ_TRANSACTIONS_DEBITS = "ReadTransactionsDebits";
        String READ_TRANSACTIONS_DETAIL = "ReadTransactionsDetail";
    }


    /*Define variable for read permissions*/
    @WebApiReadPermissionDef
    String permission;

    /*Define setter for read permissions*/
    public void setReadPermission(@WebApiReadPermissionDef String permission) {
        this.permission = permission;
    }

    /*Define getter for read permissions*/
    @WebApiReadPermissionDef
    public String getReadPermission() {
        return permission;
    }
}
