package pg.uk.openbank.webapi.model.beneficiary;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import pg.uk.openbank.webapi.model.account.CreditorAccount;

/**
 * Created by manmeet on 9/3/18.
 * {@link <ahref https://openbanking.atlassian.net/wiki/spaces/DZ/pages/5785171/Account+and+Transaction+API+Specification+-+v1.1.0/>}
 * "Beneficiary": [
 * {
 * "AccountId": "22289",
 * "BeneficiaryId": "Ben1",
 * "Reference": "Towbar Club",
 * "CreditorAccount": {
 * "SchemeName": "SortCodeAccountNumber",
 * "Identification": "80200112345678",
 * "Name": "Mrs Juniper"
 * }
 * }
 */

public class Beneficiary implements Serializable {
    @SerializedName("AccountId")
    String accountId;
    @SerializedName("BeneficiaryId")
    String beneficiaryId;
    @SerializedName("Reference")
    String reference;
    @SerializedName("CreditorAccount")
    CreditorAccount creditorAccount;

    /***************
     *Getters & Setters*
     **************/

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public CreditorAccount getCreditorAccount() {
        return creditorAccount;
    }

    public void setCreditorAccount(CreditorAccount creditorAccount) {
        this.creditorAccount = creditorAccount;
    }
}
