### Android wrapper application library - UK payment gateway using [Apigee REST apis](https://openbank.apigee.io/accounts-apis-v1-0/apis/post/account-requests#quickset-api_documentation=0) based on [Openbanking](https://openbanking.atlassian.net/wiki/spaces/DZ/pages/23363964/Read+Write+Data+API+Specifications) ###

[![](https://jitpack.io/v/org.bitbucket.manmeet/pg-uk-openbank-android.svg)](https://jitpack.io/#org.bitbucket.manmeet/pg-uk-openbank-android)

#### Version ####
* version 0.1

#### Dependency ####
* minSdkVersion     15
* targetSdkVersion  26
* compileSdkVersion 26
* buildToolsVersion 26.1.0
* [Android Base Sparta Library](https://bitbucket.org/manmeet/sparta-android-base)
* io.jsonwebtoken:jjwt:0.9.0

#### How to integrate ####
* TODO


#### Configuration ####
```java
     OpenBank.withFcaId(YOUR_FCA_FINANCIAL_ID_ISSUES_BY_OB)
                        .client(YOUR_OB_CLIENT_ID, YOUR_OB_CLIENT_SECRET)
                        .apiBaseUrl(BASE_URL_FOR_OPENBANKING_APIS) // by default = "https://apis-bank-test.apigee.net/ais/open-banking"
                        .apiVersion(OPENBSNKING_API_VERSION) //you can put this in base url and no need to add here - by default = "v1.0"
                        .redirectUri(YOUR_REDIRECT_URL_STRING_FOR_OB_APIS)
                        .enableLogging(true)
                        .config();
```
                 
#### FEATURES ####
* Create token
```java
       void tokenWebApiSample() {
               String apiUrl = "https://apis-bank-test.apigee.net/apis/v1.0/";
               new WebApiManager(context).getAccessTokenWebCall(apiUrl, ScopeDescriptor.WebApiScopeDef.ACCOUNTS, new WebHandler.OnWebCallback() {
                   @Override
                   public <T> void onSuccess(@Nullable T t, int i, Response response) {
                       
                   }
       
                   @Override
                   public <T> void onError(@Nullable T t, String s, int i, Response response) {
       
                   }
               }, true);
           }
```
* Get Accounts 
```java
       void accountsWebApiSample() {
              new WebApiManager(context).getAccountsWebCall(new WebHandler.OnWebCallback() {
                                                                            @Override
                                                                            public <T> void onSuccess(@Nullable T t, int i, Response response) {
                                                                                
                                                                            }
                                                                
                                                                            @Override
                                                                            public <T> void onError(@Nullable T t, String s, int i, Response response) {
                                                                
                                                                            }
                                                                        }, ACCESS_TOKEN, true);
          }
```
* Get Balances
```java
       void balancesWebApiSample() {
               new WebApiManager(context).getBalancesWebCall(new WebHandler.OnWebCallback() {
                                                                             @Override
                                                                             public <T> void onSuccess(@Nullable T t, int i, Response response) {
                                                                                 
                                                                             }
                                                                 
                                                                             @Override
                                                                             public <T> void onError(@Nullable T t, String s, int i, Response response) {
                                                                 
                                                                             }
                                                                         }, ACCESS_TOKEN, true);
          }
```
* Get Payment Transactions
```java
         void transactionsWebApiSample() {
                new WebApiManager(context).getTransactionsWebCall(new WebHandler.OnWebCallback() {
                                                                                  @Override
                                                                                  public <T> void onSuccess(@Nullable T t, int i, Response response) {
                                                                                      
                                                                                  }
                                                                      
                                                                                  @Override
                                                                                  public <T> void onError(@Nullable T t, String s, int i, Response response) {
                                                                      
                                                                                  }
                                                                              }, ACCESS_TOKEN, true);
          }
```
* Get Beneficiaries
```java
         void beneficiariesWebApiSample() {
                new WebApiManager(context).getBeneficiariesWebCall(new WebHandler.OnWebCallback() {
                                                                                   @Override
                                                                                   public <T> void onSuccess(@Nullable T t, int i, Response response) {
                                                                                       
                                                                                   }
                                                                       
                                                                                   @Override
                                                                                   public <T> void onError(@Nullable T t, String s, int i, Response response) {
                                                                       
                                                                                   }
                                                                               }, ACCESS_TOKEN, true);
          }
```

#### Callbacks ####
```java 
implements WebHandler.OnWebCallback
```
* `onSuccess()`
* `onError()`


#### Who do I talk to? ####
* androidproductteam@yapits.com